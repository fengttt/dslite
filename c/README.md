# dslite

## Build and install

First build/install dependencies.   All of them are strightforward on Linux.  For example, on Ubuntu, one
need to apt install uuid-dev, libgoogle-glog-dev, libssl-dev.

* sqlite3: I use the following
```
gcc -O2 -I. -DSQLITE_ENABLE_FTS5 -DSQLITE_ENABLE_JSON1 -DHAVE_READLINE shell.c sqlite3.c -ldl -lpthread -lreadline -lncurses -o sqlite3
gcc -O2 -I. -DSQLITE_ENABLE_FTS5 -DSQLITE_ENABLE_JSON1 -DHAVE_READLINE -fPIC --shared sqlite3.c -o libsqlite3.so
```

* sketches-cpp

* or-tools: I use the binary release

* you need to install python3/pip3, python3 tablulate, libgoogle-glog-dev, go-sqlite3, for python and go language 
binding.   

## source env.sh, make, make install.

