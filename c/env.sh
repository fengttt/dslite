DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"

export DSLITE_DIR=$DIR
export SQLITE3_DIR=$HOME/oss/sqlite3
export DATASKETCHES_DIR=$HOME/oss/datasketches-cpp
export ORTOOLS_DIR=$HOME/oss/or-tools

export PATH=$SQLITE3_DIR:$PATH
export LD_LIBRARY_PATH=$DSLITE_DIR/lib:$SQLITE3_DIR:$ORTOOLS_DIR/lib:$LD_LIBRARY_PATH
