CFLAGS = -fPIC -shared -Wall -pthread -Wno-sign-compare -I$(SQLITE3_DIR) 
# to compile for debug: make DEBUG=1
# to compile for no debug: make
ifdef DEBUG
    CFLAGS += -O0 -g
else
    CFLAGS += -O2 -g -DNDEBUG
endif

CXXFLAGS += $(CFLAGS) -I${DATASKETCHES_DIR} -I${DATASKETCHES_DIR}/common/include

LDFLAGS = -L${DSLITE_DIR}/src/util -ldslutil -L${SQLITE3_DIR} -lsqlite3
LDFLAGS += -lssl -lcrypto
