/*
 * SQLite csv external table.
 */

#include <sqlite3ext.h>
SQLITE_EXTENSION_INIT1

#include <cstring>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>

#include "../util/csv_parser.hpp"
#include "../util/ds_tuple.hpp"


/* Skip leading whitespace.  Return a pointer to the first non-whitespace
** character, or to the zero terminator if the string has only whitespace */
static const char *csv_skip_whitespace(const char *z){
  while( isspace((unsigned char)z[0]) ) z++;
  return z;
}

/* Remove trailing whitespace from the end of string z[] */
static void csv_trim_whitespace(char *z){
  size_t n = strlen(z);
  while( n>0 && isspace((unsigned char)z[n]) ) n--;
  z[n] = 0;
}

/* Dequote the string */
static void csv_dequote(char *z){
  int j;
  char cQuote = z[0];
  size_t i, n;

  if( cQuote!='\'' && cQuote!='"' ) return;
  n = strlen(z);
  if( n<2 || z[n-1]!=z[0] ) return;
  for(i=1, j=0; i<n-1; i++){
    if( z[i]==cQuote && z[i+1]==cQuote ) i++;
    z[j++] = z[i];
  }
  z[j] = 0;
}



/* Check to see if the string is of the form:  "TAG = VALUE" with optional
** whitespace before and around tokens.  If it is, return a pointer to the
** first character of VALUE.  If it is not, return NULL.
*/
static const char *csv_parameter(const char *zTag, int nTag, const char *z){
  z = csv_skip_whitespace(z);
  if( strncmp(zTag, z, nTag)!=0 ) return 0;
  z = csv_skip_whitespace(z+nTag);
  if( z[0]!='=' ) return 0;
  return csv_skip_whitespace(z+1);
}

static int csv_string_parameter(
  const char *zParam,      /* Parameter we are checking for */
  const char *zArg,        /* Raw text of the virtual table argment */
  char **pzVal             /* Write the dequoted string value here */
){
  const char *zValue;
  zValue = csv_parameter(zParam,(int)strlen(zParam),zArg);

  if( zValue==0 ) {
	  return 0;
  }

  if( *pzVal ){
	  return 2;
  }

  *pzVal = sqlite3_mprintf("%s", zValue);
  // check pzVal OOM,
  csv_trim_whitespace(*pzVal);
  csv_dequote(*pzVal);
  return 1;
}

#if 0

/* Return 0 if the argument is false and 1 if it is true.  Return -1 if
** we cannot really tell.
*/
static int csv_boolean(const char *z){
  if( sqlite3_stricmp("yes",z)==0
   || sqlite3_stricmp("on",z)==0
   || sqlite3_stricmp("true",z)==0
   || (z[0]=='1' && z[1]==0)
  ){
    return 1;
  }
  if( sqlite3_stricmp("no",z)==0
   || sqlite3_stricmp("off",z)==0
   || sqlite3_stricmp("false",z)==0
   || (z[0]=='0' && z[1]==0)
  ){
    return 0;
  }
  return -1;
}

/* Check to see if the string is of the form:  "TAG = BOOLEAN" or just "TAG".
** If it is, set *pValue to be the value of the boolean ("true" if there is
** not "= BOOLEAN" component) and return non-zero.  If the input string
** does not begin with TAG, return zero.
*/
static int csv_boolean_parameter(
  const char *zTag,       /* Tag we are looking for */
  int nTag,               /* Size of the tag in bytes */
  const char *z,          /* Input parameter */
  int *pValue             /* Write boolean value here */
){
  int b;
  z = csv_skip_whitespace(z);
  if( strncmp(zTag, z, nTag)!=0 ) return 0;
  z = csv_skip_whitespace(z + nTag);
  if( z[0]==0 ){
    *pValue = 1;
    return 1;
  }
  if( z[0]!='=' ) return 0;
  z = csv_skip_whitespace(z+1);
  b = csv_boolean(z);
  if( b>=0 ){
    *pValue = b;
    return 1;
  }
  return 0;
}
#endif

	


/* Forward references to the various virtual table methods implemented
** in this file. */
static int csvtabCreate(sqlite3*, void*, int, const char*const*, 
                           sqlite3_vtab**,char**);
static int csvtabConnect(sqlite3*, void*, int, const char*const*, 
                           sqlite3_vtab**,char**);
static int csvtabBestIndex(sqlite3_vtab*,sqlite3_index_info*);
static int csvtabDisconnect(sqlite3_vtab*);
static int csvtabOpen(sqlite3_vtab*, sqlite3_vtab_cursor**);
static int csvtabClose(sqlite3_vtab_cursor*);
static int csvtabFilter(sqlite3_vtab_cursor*, int idxNum, const char *idxStr,
                          int argc, sqlite3_value **argv);
static int csvtabNext(sqlite3_vtab_cursor*);
static int csvtabEof(sqlite3_vtab_cursor*);
static int csvtabColumn(sqlite3_vtab_cursor*,sqlite3_context*,int);
static int csvtabRowid(sqlite3_vtab_cursor*,sqlite3_int64*);

typedef struct CsvTable {
	sqlite3_vtab base;			// Base, must be first.
	std::string *filename;
	std::string *execute;
	std::string *coltypes;
	int nhdr;

	bool hasdelim;
	char delim;
	bool hasquote;
	char quote;
	bool hasescape;
	char escape;
	bool hascommet;
	char comment;
	std::string *nullstr;
} CsvTable;

typedef struct CsvCursor {
	sqlite3_vtab_cursor base;	// Base, must be first.
	dslite::CsvParser *parser;	// our parser.
	const char **azVal;
	dslite::Tuple *tuple;
	sqlite3_int64 iRowid;
} CsvCursor;

#define SET_ERR_MSG(pzErr, ...)			\
	if (1) {							\
		sqlite3_free(*pzErr);			\
		*pzErr = sqlite3_mprintf(__VA_ARGS__);	\
	} else (void) 0
		
	
static void set_parser_option(dslite::CsvParser *parser, CsvTable *pTab)
{
	if (pTab->hasdelim) {
		parser->set_delim(pTab->delim);
	}
	if (pTab->hasquote) {
		parser->set_quote(pTab->quote);
	}
	if (pTab->hasescape) {
		parser->set_escape(pTab->escape);
	}
	if (pTab->hascommet) {
		parser->set_comment(pTab->comment); 
	}
	if (pTab->nullstr) {
		parser->set_nullstr(pTab->nullstr->c_str());
	}
}

static int csvtabDisconnect(sqlite3_vtab *pt) 
{
	CsvTable *pcsvt = (CsvTable *) pt;
	if (pcsvt->filename) { 
		delete pcsvt->filename; 
		pcsvt->filename = 0;
	}
	if (pcsvt->execute) { 
		delete pcsvt->execute; 
		pcsvt->execute = 0;
	}

	if (pcsvt->coltypes) {
		delete pcsvt->coltypes;
		pcsvt->coltypes = 0;
	}

	if (pcsvt->nullstr) { 
		delete pcsvt->nullstr; 
		pcsvt->nullstr = 0;
	}

	sqlite3_free(pt);
	return SQLITE_OK;
}

enum {
	PARAM_FILENAME = 0,
	PARAM_EXECUTE,
	PARAM_COLTYPES,
	PARAM_DELIMITER,
	PARAM_QUOTE,
	PARAM_ESCAPE,
	PARAM_COMMENT,
	PARAM_NULLSTR,
	PARAM_CNT
};

static int csvtabConnect(
		sqlite3 *db,
		void *pAux,
		int argc, const char *const*argv,	/* wow.  what an argv */
		sqlite3_vtab **ppvt,
		char **pzErr)
{
	CsvTable *pNew = 0;
	dslite::CsvParser *pParser = 0;

	int nCol = -1;
	int nHeader = -1;
	const char **headerCols = 0;
	int rc;

	static const char *azParam[] = {
		"filename", "execute", "coltypes", "delimiter", "quote", "escape", "comment", "nullstr"
	};
	char *azPValue[PARAM_CNT]; 
	memset(azPValue, 0, sizeof(azPValue));

	for (int i = 3; i < argc; i++) {
		const char* z = argv[i];
		const char* zValue;
		int nfound = 0;
		for (int j = 0; j < PARAM_CNT; j++) {
			nfound = csv_string_parameter(azParam[j], z, &azPValue[j]);
			if (nfound == 1) {
				break;
			} else if (nfound > 1) {
				SET_ERR_MSG(pzErr, "more than one '%s' parameter", azParam[j]);
				goto csvtab_connect_error;
			} 
		}
		if (nfound) {
			continue;
		}

		if ( (zValue = csv_parameter("header", 6, z)) != 0) { 
			if (nHeader > 0) {
				SET_ERR_MSG(pzErr, "more than one 'header' parameter");
				goto csvtab_connect_error;
			}
			nHeader = atoi(zValue);
			if (nHeader <= 0) {
				SET_ERR_MSG(pzErr, "invalid 'header' parameter -- must be non negtive int.");
				goto csvtab_connect_error;
			}
			continue;
		}

		if ( (zValue = csv_parameter("columns", 7, z)) != 0) {
			if (nCol > 0) {
				SET_ERR_MSG(pzErr, "more than one 'columns' parameter");
				goto csvtab_connect_error;
			}
			nCol = atoi(zValue);
			if (nCol <= 0) {
				SET_ERR_MSG(pzErr, "invalid 'columns' parameter -- must be positive int."); 
				goto csvtab_connect_error;
			}
			continue;
		}

		SET_ERR_MSG(pzErr, "invalid parameter '%s'", z); 
		goto csvtab_connect_error;
	}

	if ((azPValue[PARAM_FILENAME] == 0) == (azPValue[PARAM_EXECUTE] == 0)) {
		SET_ERR_MSG(pzErr, "must specifiy filename= or execute= but not both."); 
		goto csvtab_connect_error;
	}

	pNew = (CsvTable *) sqlite3_malloc(sizeof(*pNew));
	*ppvt = (sqlite3_vtab*) pNew;

	if (pNew == 0) {
		goto csvtab_connect_error;
	}
	memset(pNew, 0, sizeof(*pNew));

	if (azPValue[PARAM_FILENAME]) {
		pNew->filename = new std::string(azPValue[PARAM_FILENAME]);
	} else {
		pNew->execute = new std::string(azPValue[PARAM_EXECUTE]); 
	}

	if (azPValue[PARAM_COLTYPES]) {
		pNew->coltypes = new std::string(azPValue[PARAM_COLTYPES]);
		if (nCol <= 0) {
			nCol = pNew->coltypes->size();
		} else if (nCol != pNew->coltypes->size()) {
			SET_ERR_MSG(pzErr, "coltypes and number of columns mismatch."); 
			goto csvtab_connect_error;
		}
	}
	if (azPValue[PARAM_DELIMITER]) {
		pNew->hasdelim = true;
		pNew->delim = azPValue[PARAM_DELIMITER][0];
	}
	if (azPValue[PARAM_QUOTE]) {
		pNew->hasquote = true;
		pNew->quote = azPValue[PARAM_QUOTE][0];
	}
	if (azPValue[PARAM_ESCAPE]) {
		pNew->hasescape = true;
		pNew->escape = azPValue[PARAM_ESCAPE][0];
	}
	if (azPValue[PARAM_COMMENT]) { 
		pNew->hascommet = true;
		pNew->comment = azPValue[PARAM_COMMENT][0];
	}
	if (azPValue[PARAM_NULLSTR]) {
		pNew->nullstr = new std::string(azPValue[PARAM_NULLSTR]); 
	}

	if (nHeader > 0) {
		pNew->nhdr = nHeader; 
	} else {
		pNew->nhdr = 0;
	}

	if (nCol <= 0) {
		// try to pull schema col from header.   We only run parser 
		// for csv files.   execute could be expensive, and execute
		// more than once may surprise user.
		if (!pNew->filename) {
			SET_ERR_MSG(pzErr, "execute csv table require nCol.");
			goto csvtab_connect_error;
		}

		pParser = dslite::CsvParser::create_file_parser(
				azPValue[PARAM_FILENAME], nCol); 

		if (!pParser) {
			SET_ERR_MSG(pzErr, "cannot create parser to derive schema from header.");
			goto csvtab_connect_error;
		} 
		set_parser_option(pParser, pNew);

		if (!pParser->start()) {
			SET_ERR_MSG(pzErr, "cannot start parser to derive schema from header.");
			goto csvtab_connect_error;
		}

		if (!pParser->next(&headerCols)) {
			SET_ERR_MSG(pzErr, "cannot read first line to derive schema from header.");
			goto csvtab_connect_error;
		} 

		nCol = pParser->get_nfield();
		if (nHeader <= 0) {
			headerCols = 0;
		}
	}

	if (!pNew->coltypes) {
		pNew->coltypes = new std::string(nCol, 't');
	}

	// Build schema
	{
		sqlite3_str *pStr = sqlite3_str_new(0);
		sqlite3_str_appendf(pStr, "CREATE TABLE x(");
		const char *zSep = "";

		for (int ii = 0; ii < nCol; ii++) {
			const char *ct = dslite::colt2sqlt(pNew->coltypes->at(ii)); 
			if (headerCols) {
				sqlite3_str_appendf(pStr, "%s\"%w\" %s", zSep, headerCols[ii], ct); 
			} else {
				sqlite3_str_appendf(pStr, "%sc%d %s", zSep, ii, ct); 
			}
			zSep = ",";
		}
		sqlite3_str_appendf(pStr, ")");
		char* pSchema = (char*) sqlite3_str_finish(pStr);

		rc = sqlite3_declare_vtab(db, pSchema); 
		if (rc) {
			SET_ERR_MSG(pzErr, "bad schema: '%s'"); 
			sqlite3_free(pSchema);
			goto csvtab_connect_error;
		}
		sqlite3_free(pSchema);
	}

	if (pParser) {
		delete pParser;
		pParser = 0;
	}

	for (int i = 0; i < PARAM_CNT; i++) {
		sqlite3_free(azPValue[i]);
	}

	return SQLITE_OK;

csvtab_connect_error:
	if (pParser) {
		delete pParser;
		pParser = 0;
	}

	if (pNew) {
		// disconnect frees pNew as well
		csvtabDisconnect(&pNew->base);
	}

	for (int i = 0; i < PARAM_CNT; i++) {
		sqlite3_free(azPValue[i]);
	}

	return SQLITE_ERROR;
}

/*
** The xConnect and xCreate methods do the same thing, but they must be
** different so that the virtual table is not an eponymous virtual table.
*/
static int csvtabCreate(
  sqlite3 *db,
  void *pAux,
  int argc, const char *const*argv,
  sqlite3_vtab **ppVtab,
  char **pzErr
){
 return csvtabConnect(db, pAux, argc, argv, ppVtab, pzErr);
}

static void csvtab_close_cursor(CsvCursor *pCur)
{
	if (pCur->parser) {
		delete pCur->parser;
		pCur->parser = 0;
	}

	if (pCur->tuple) {
		delete pCur->tuple;
		pCur->tuple = 0;
	}
	pCur->azVal = 0;
	pCur->iRowid = 0;
}

static int csvtab_open_cursor(CsvTable *pTab, CsvCursor *pCur)
{
	if (pTab->filename) {
		pCur->parser = dslite::CsvParser::create_file_parser(
				pTab->filename->c_str(), pTab->coltypes->size());
	} else if (pTab->execute) {
		pCur->parser = dslite::CsvParser::create_pipe_parser(
				pTab->execute->c_str(), pTab->coltypes->size()); 
	}

	if (!pCur->parser) {
		SET_ERR_MSG(&pTab->base.zErrMsg, "cannot create csv parser.");
		return SQLITE_ERROR;
	}

	if (!pTab->coltypes) {
		SET_ERR_MSG(&pTab->base.zErrMsg, "csv table does not have coltypes."); 
		return SQLITE_ERROR;
	}

	pCur->tuple = new dslite::Tuple(); 

	set_parser_option(pCur->parser, pTab);
	if (!pCur->parser->start()) {
		SET_ERR_MSG(&pTab->base.zErrMsg, "cannot start csv parser"); 
		return SQLITE_ERROR;
	} 

	for (int i = 0; i < pTab->nhdr; i++) {
		if (!pCur->parser->next(&pCur->azVal)) {
			SET_ERR_MSG(&pTab->base.zErrMsg, "csv parser cannot skip header."); 
			return SQLITE_ERROR;
		} 
	}
	pCur->azVal = 0;
	pCur->iRowid = 0;
	return SQLITE_OK;
}

static int csvtabClose(sqlite3_vtab_cursor *cur)
{
	// CsvTable *pTab = (CsvTable*)pCur->base.pVtab;
	CsvCursor *pCur = (CsvCursor *) cur;
	csvtab_close_cursor(pCur);
	sqlite3_free(cur);
	return SQLITE_OK;
}

static int csvtabOpen(sqlite3_vtab *p, sqlite3_vtab_cursor **ppCur)
{
	// CsvTable *pTab = (CsvTable *) p;
	CsvCursor *pCur = 0;
	pCur = (CsvCursor *) sqlite3_malloc(sizeof(CsvCursor));
	if (pCur == 0) {
		return SQLITE_NOMEM;
	}

	memset(pCur, 0, sizeof(CsvCursor));
	*ppCur = &pCur->base;
	return SQLITE_OK;
}

static int csvtabNext(sqlite3_vtab_cursor *cur)
{
	CsvCursor *pCur = (CsvCursor*)cur;
	CsvTable *pTab = (CsvTable*) pCur->base.pVtab;

	if (pCur->iRowid >= 0) {
		// CsvTable *pTab = (CsvTable*)cur->pVtab;
		bool ok = pCur->parser->next(&pCur->azVal);
		if (!ok) {
			pCur->azVal = 0;
			pCur->iRowid = -1;
		} else {
			pCur->tuple->build(*pTab->coltypes, pCur->azVal);
			pCur->iRowid++;
		}
	}
	return SQLITE_OK;
}

static int csvtabColumn(
		sqlite3_vtab_cursor *cur,   /* The cursor */
		sqlite3_context *ctx,       /* First argument to sqlite3_result_...() */
		int i                       /* Which column to return */
		) 
{
	CsvCursor *pCur = (CsvCursor*)cur;
	CsvTable *pTab = (CsvTable*)cur->pVtab;
	int err = pCur->tuple->get_col(*pTab->coltypes, i, ctx); 
	if (err < 0) {
		sqlite3_result_error(ctx, "Error when parsing csv field", -1); 
		return SQLITE_ERROR;
	}
	return SQLITE_OK;
}
		
/*
** Return the rowid for the current row.
*/
static int csvtabRowid(sqlite3_vtab_cursor *cur, sqlite_int64 *pRowid){
  CsvCursor *pCur = (CsvCursor*)cur;
  *pRowid = pCur->iRowid;
  return SQLITE_OK;
}

/*
** Return TRUE if the cursor has been moved off of the last
** row of output.
*/
static int csvtabEof(sqlite3_vtab_cursor *cur){
  CsvCursor *pCur = (CsvCursor*)cur;
  return pCur->iRowid<0;
}

static int csvtabFilter(
		sqlite3_vtab_cursor *pVtabCursor, 
		int idxNum, const char *idxStr,
		int argc, sqlite3_value **argv
		)
{
	int rc;
	CsvCursor *pCur = (CsvCursor*)pVtabCursor;
	CsvTable *pTab = (CsvTable*)pVtabCursor->pVtab;

	csvtab_close_cursor(pCur);
	rc = csvtab_open_cursor(pTab, pCur);
	if (rc!=SQLITE_OK) {
		return rc;
	}

	return csvtabNext(pVtabCursor);
}

static int csvtabBestIndex(
		sqlite3_vtab *tab,
		sqlite3_index_info *pIdxInfo
)
{
	pIdxInfo->estimatedCost = 1000000;
	return SQLITE_OK;
}

static sqlite3_module dslitecsv_module = {
	0,					/* iVersion */
	csvtabCreate,		
	csvtabConnect,	
	csvtabBestIndex,	
	csvtabDisconnect,
	csvtabDisconnect,	/* xDestroy, same as Disconnect */
	csvtabOpen, 
	csvtabClose,
	csvtabFilter,
	csvtabNext,
	csvtabEof,
	csvtabColumn,
	csvtabRowid,
	0,					// xUpdate
	0,					// xBegin 
	0,					// xSync 
	0,					// xCommit
	0,					// xRollback
	0,					// xFindMethod
	0,					// xRename
};


extern "C"
int sqlite3_dslitecsv_init(
		sqlite3 *db,
		char **pzErrMsg,
		const sqlite3_api_routines *pApi
		)
{
	int rc;
	SQLITE_EXTENSION_INIT2(pApi);
	rc = sqlite3_create_module(db, "dslite_csv", &dslitecsv_module, 0);
	return rc;
}


