#include "sqlite3ext.h"
SQLITE_EXTENSION_INIT1

#include <string>
#include <cstring>
#include <vector>

#include "../util/wormhole.hpp"
#include "../util/ds_hash.hpp"
#include "../util/ds_encode.h"

struct wCursor {
	sqlite3_vtab_cursor base;
	std::vector<std::string> *kvec; 
	std::vector<int64_t> *tsvec;
	sqlite3_int64 iRowid;
};

void wcursor_open(wCursor *p) 
{
	p->kvec = new std::vector<std::string>();
	p->tsvec = new std::vector<int64_t>();
	dslite::wormhole_list(*p->kvec, *p->tsvec);
	p->iRowid = 0;
}

void wcursor_close(wCursor *p) 
{
	if (p->kvec) {
		delete p->kvec;
		p->kvec = 0;
	}
	if (p->tsvec) {
		delete p->tsvec;
		p->tsvec = 0;
	}
	p->iRowid = 0;
}

static int wConnect (
		sqlite3 *db, void *pAux,
		int argc, const char *const*argv,
		sqlite3_vtab **ppt,
		char **pzErr)
{
	sqlite3_vtab *pNew;
	int rc = sqlite3_declare_vtab(db, "CREATE TABLE x(k text, ts timestamp)");
	if (rc == SQLITE_OK) {
		pNew = (sqlite3_vtab *) sqlite3_malloc(sizeof(*pNew));
		*ppt = pNew;
		if (pNew == 0) {
			return SQLITE_NOMEM;
		}
		memset(pNew, 0, sizeof(*pNew));
	}
	return rc;
}

static int wDisconnect(sqlite3_vtab *pt) 
{
	sqlite3_free(pt);
	return SQLITE_OK;
}

static int wOpen(sqlite3_vtab* pt, sqlite3_vtab_cursor **ppCur)
{
	wCursor *pCur = (wCursor *) sqlite3_malloc(sizeof(wCursor));
	if (pCur == 0) {
		return SQLITE_NOMEM;
	}
	memset(pCur, 0, sizeof(*pCur));
	*ppCur = &pCur->base;
	return SQLITE_OK;
}

static int wClose(sqlite3_vtab_cursor *cur) 
{
	wcursor_close((wCursor *) cur);
	sqlite3_free(cur);
	return SQLITE_OK;
}

static int wNext(sqlite3_vtab_cursor *cur) {
	wCursor *pCur = (wCursor *) cur;
	pCur->iRowid++;
	return SQLITE_OK;
}

static int wColumn(
		sqlite3_vtab_cursor *cur,
		sqlite3_context *ctx,
		int i)
{
	wCursor *pCur = (wCursor *) cur;
	if (i == 0) {
		const char* pk = (*(pCur->kvec))[pCur->iRowid-1].c_str();
		sqlite3_result_text(ctx, pk, -1, SQLITE_STATIC);
	} else {
		int64_t ts = (*(pCur->tsvec))[pCur->iRowid-1];
		ts /= 1000000000;
		sqlite3_result_int64(ctx, ts); 
	}
	return SQLITE_OK;
}

static int wRowid(sqlite3_vtab_cursor *cur, sqlite3_int64 *pRowid) 
{
	wCursor *pCur = (wCursor *) cur;
	*pRowid = pCur->iRowid;
	return SQLITE_OK;
}

static int wEof(sqlite3_vtab_cursor *cur)
{
	wCursor *pCur = (wCursor *) cur;
	if (pCur->iRowid > (int) pCur->kvec->size()) {
		return true;
	}
	return false;
}

static int wFilter(
		sqlite3_vtab_cursor *cur, 
		int idxNum, const char *idxStr,
		int argc, sqlite3_value **argv)
{
	wCursor *pCur = (wCursor *) cur;
	wcursor_close(pCur);
	wcursor_open(pCur); 
	return wNext(cur); 
}

static int wBestIndex(sqlite3_vtab *pt, sqlite3_index_info *pIdxInfo)
{
	pIdxInfo->estimatedCost = 1000;
	return SQLITE_OK;
}


static sqlite3_module dslitewormhole_module = {
	0,					/* iVersion */
	0,					/* xCreate */
	wConnect,	
	wBestIndex,	
	wDisconnect,
	0,					/* xDestroy */ 
	wOpen, 
	wClose,
	wFilter,
	wNext,
	wEof,
	wColumn,
	wRowid,
	0,					// xUpdate
	0,					// xBegin 
	0,					// xSync 
	0,					// xCommit
	0,					// xRollback
	0,					// xFindMethod
	0,					// xRename
};

static void wDel(sqlite3_context *ctxt,
		int argc, sqlite3_value **argv)
{
	if (argc!=1 || sqlite3_value_type(argv[0]) != SQLITE_TEXT) {
		sqlite3_result_error(ctxt, "Invalud arguments.", -1);
		return;
	}

	const char *pk = (const char *)sqlite3_value_text(argv[0]);
	std::string keystr(pk);
	bool ok = dslite::wormhole_remove(keystr);
	sqlite3_result_int(ctxt, ok ? 1 : 0);
}

static void xxHash(sqlite3_context *ctxt,
		int argc, sqlite3_value **argv)
{
	if (argc!=1) { 
		sqlite3_result_error(ctxt, "Invalud arguments.", -1);
	}

	if (sqlite3_value_type(argv[0]) == SQLITE_NULL) { 
		return;
	}

	int nb = sqlite3_value_bytes(argv[0]);
	const char *ss = (const char *)sqlite3_value_text(argv[0]);
	uint64_t xxh = dslite::xxhash(ss, nb);
	sqlite3_result_int64(ctxt, xxh); 
}

static void sha256Hash(sqlite3_context *ctxt,
		int argc, sqlite3_value **argv)
{
	if (argc!=1) { 
		sqlite3_result_error(ctxt, "Invalud arguments.", -1);
	}

	if (sqlite3_value_type(argv[0]) == SQLITE_NULL) { 
		return;
	}

	int nb = sqlite3_value_bytes(argv[0]);
	const char *ss = (const char *)sqlite3_value_text(argv[0]);
	char md[32];
	char hex[65];
	dslite::sha256(ss, nb, md);
	hex_encode(md, 32, hex);
	hex[64] = 0;
	sqlite3_result_text(ctxt, hex, -1, SQLITE_TRANSIENT);
}



#define CHECK_RC					\
	if (rc != SQLITE_OK) {			\
		return rc;					\
	} else (void) 0								

extern "C"
int sqlite3_dslitemisc_init(
		sqlite3 *db,
		char **pzErrMsg,
		const sqlite3_api_routines *pApi
		)
{
	int rc;
	SQLITE_EXTENSION_INIT2(pApi);
	rc = sqlite3_create_module(db, "dslite_wormhole", &dslitewormhole_module, 0);
	CHECK_RC;

	rc = sqlite3_create_function(db, "dslite_wormhole_del", 1, SQLITE_UTF8, 0, wDel, 0, 0);
	CHECK_RC;

	rc = sqlite3_create_function(db, "dslite_xxhash", 1, SQLITE_UTF8, 0, xxHash, 0, 0);
	CHECK_RC;

	rc = sqlite3_create_function(db, "dslite_sha256", 1, SQLITE_UTF8, 0, sha256Hash, 0, 0);
	CHECK_RC;

	return rc;
}

