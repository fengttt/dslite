#include "sqlite3ext.h"
SQLITE_EXTENSION_INIT1

#include "../util/ds_sqlite.h"

#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>
#include <sstream>

#include "../util/wormhole.hpp"
#include "absl/base/port.h"
#include "ortools/graph/graph.h"

struct DsliteGraph {
	char gspec[4];

	util::ListGraph<> *lg; 
	util::ReverseArcListGraph<> *rlg; 
	util::StaticGraph<> *sg;
	util::ReverseArcStaticGraph<> *rsg;

	std::unordered_map<std::string, int> s2i;
	std::vector<std::string> i2s;

	std::vector<int64_t> iWeights;
	std::vector<double> fWeights;

	static bool isStaticG(const char* c) { return c[0] == 's'; }
	static bool isReverseArcStaticG(const char* c) { return c[0] == 'S'; }
	static bool isListG(const char* c) { return c[0] == 'l'; }
	static bool isReverseArcListG(const char* c) { return c[0] == 'L'; }
	static bool isGTValid(const char *c) { return isStaticG(c) || isReverseArcStaticG(c) || isListG(c) || isReverseArcListG(c); }
	static bool isIntNode(const char *c) { return c[1] == 'i'; }
	static bool isStrNode(const char *c) { return c[1] == 's'; }
	static bool isNodeTValid(const char *c) { return isIntNode(c) || isStrNode(c); }
	static bool isI64Weighted(const char *c) { return c[2] == 'I'; }
	static bool isF64Weighted(const char *c) { return c[2] == 'F'; }
	static bool isNotWeighted(const char *c) { return c[2] == 'N'; }
	static bool isWeightValid(const char *c) { return isI64Weighted(c) || isF64Weighted(c) || isNotWeighted(c); }

	static bool isValidGraphSpec(const char *gs) {
		if (strlen(gs) != 3) {
			return false;
		}
		return isGTValid(gs) && isNodeTValid(gs) && isWeightValid(gs);
	}

	bool needMapNode() { return isStrNode(gspec); }
	bool i64Weighted() { return isI64Weighted(gspec); }
	bool f64Weighted() { return isF64Weighted(gspec); }
	bool isWeighted() { return i64Weighted() || f64Weighted(); }
	bool staticGraph() { return isStaticG(gspec) || isReverseArcStaticG(gspec); }
	bool isReverseG() { return isReverseArcStaticG(gspec) || isReverseArcListG(gspec); } 
	bool isDirected() { return !isReverseG(); }

	DsliteGraph(const char* gs) {
		gspec[0] = gs[0];
		gspec[1] = gs[1];
		gspec[2] = gs[2];
		gspec[3] = 0;

		lg = 0; rlg = 0; sg = 0; rsg = 0;

		switch (gspec[0]) {
			case 's':
				sg = new util::StaticGraph<> ();
				break;
			case 'S':
				rsg = new util::ReverseArcStaticGraph<> ();
				break;
			case 'l':
				lg = new util::ListGraph<> ();
				break;
			case 'L':
				rlg = new util::ReverseArcListGraph<> ();
				break;
		}
	}

	~DsliteGraph() {
		if (sg) { delete sg; }
		if (rsg) { delete rsg; }
		if (lg) { delete lg; }
		if (rlg) { delete rlg; }
	}

	int num_nodes() {
		if (sg) { return sg->num_nodes(); } 
		if (rsg) { return rsg->num_nodes(); } 
		if (lg) { return lg->num_nodes(); } 
		if (rlg) { return rlg->num_nodes(); } 
		return 0;
	}

	int num_arcs() {
		if (sg) { return sg->num_arcs(); } 
		if (rsg) { return rsg->num_arcs(); } 
		if (lg) { return lg->num_arcs(); } 
		if (rlg) { return rlg->num_arcs(); } 
		return 0;
	}

	int Tail(int arc) { 
		if (sg) { return sg->Tail(arc); } 
		if (rsg) { return rsg->Tail(arc); } 
		if (lg) { return lg->Tail(arc); } 
		if (rlg) { return rlg->Tail(arc); } 
		return -1;
	}

	int Head(int arc) { 
		if (sg) { return sg->Head(arc); } 
		if (rsg) { return rsg->Head(arc); } 
		if (lg) { return lg->Head(arc); } 
		if (rlg) { return rlg->Head(arc); } 
		return -1;
	}

	int64_t iWeight(int arc) {
		return iWeights[arc];
	}
	double fWeight(int arc) {
		return fWeights[arc];
	}


	int addArc(int argc, sqlite3_value **argv) {
		if (needMapNode()) {
			const char* ptail = (const char *) sqlite3_value_text(argv[0]);
			const char* phead = (const char *) sqlite3_value_text(argv[1]);
			if (i64Weighted()) {
				if (argc != 3) { return -1; }
				return addArcIW(ptail, phead, sqlite3_value_int64(argv[2]));
			} else if (f64Weighted()) {
				if (argc != 3) { return -1; }
				return addArcFW(ptail, phead, sqlite3_value_double(argv[2]));
			} else {
				if (argc != 2) { return -1; }
				return addArc(ptail, phead);
			}
		} else {
			int tail = sqlite3_value_int(argv[0]);
			int head = sqlite3_value_int(argv[1]);
			if (i64Weighted()) {
				if (argc != 3) { return -1; }
				return addArcIW(tail, head, sqlite3_value_int64(argv[2]));
			} else if (f64Weighted()) {
				if (argc != 3) { return -1; }
				return addArcFW(tail, head, sqlite3_value_double(argv[2]));
			} else {
				if (argc != 2) { return -1; }
				return addArc(tail, head);
			}
		}
	}
	
	int addArc(int tail, int head) {
		if (sg) { return sg->AddArc(tail, head); }
		if (rsg) { return rsg->AddArc(tail, head); }
		if (lg) { return lg->AddArc(tail, head); }
		if (rlg) { return rlg->AddArc(tail, head); }
		return -1;
	}

	int addArcIW(int tail, int head, int64_t w)
	{
		int arc = addArc(tail, head);
		iWeights.push_back(w);
		return arc;
	}

		 
	int addArcFW(int tail, int head, double w)
	{
		int arc = addArc(tail, head);
		fWeights.push_back(w);
		return arc;
	}

	int addArc(const char* tail, const char* head) 
	{
		int itail, ihead;
		std::unordered_map<std::string, int>::iterator it = s2i.find(tail);
		if (it != s2i.end()) {
			itail = (*it).second;
		} else {
			itail = (int) i2s.size();
			i2s.push_back(tail);
			s2i[std::string(tail)] = itail;
		}

		it = s2i.find(head);
		if (it != s2i.end()) {
			ihead = (*it).second;
		} else {
			ihead = (int) i2s.size();
			i2s.push_back(head);
			s2i[std::string(head)] = ihead; 
		}

		return addArc(itail, ihead);
	}

	int addArcIW(const char* tail, const char* head, int64_t w)
	{
		int arc = addArc(tail, head);
		iWeights.push_back(w);
		return arc;
	}

	int addArcFW(const char* tail, const char* head, double w)
	{
		int arc = addArc(tail, head);
		fWeights.push_back(w);
		return arc;
	}

	int build() {
		if (!staticGraph()) {
			return 0;
		}

		std::vector<int> perm; 
		if (i64Weighted() || f64Weighted()) {
			if (sg) {
				sg->Build(&perm);
			} else {
				rsg->Build(&perm); 
			}
			if (i64Weighted()) {
				util::Permute(perm, &iWeights);
			} else {
				util::Permute(perm, &fWeights);
			}
		} else {
			if (sg) {
				sg->Build();
			} else {
				rsg->Build();
			}
		}
		return 1;
	}

	void dotWeight(std::ostream& os, int arc) {
		if (i64Weighted()) {
			os << "[label=\"" << iWeights[arc] << "\",\"weight=\"" << iWeights[arc] << "\"]";
		} else if (f64Weighted()) {
			os << "[label=\"" << fWeights[arc] << "\",\"weight=\"" << fWeights[arc] << "\"]";
		}
	}

	void dotNode(std::ostream& os, int node) {
		if (!needMapNode()) {
			os << node;
		} else {
			os << "\"" << i2s[node] << "\"";
		}
	}

	void dot(std::ostream& os) {
		std::string edge;
		if (isDirected()) {
			os << "digraph {" << std::endl;
			edge = " -> ";
		} else {
			os << "graph {" << std::endl;
			edge = " -- ";
		}

		if (!needMapNode()) {
			for (int i = 0; i < num_arcs(); i++) {
				int h = Head(i);
				int t = Tail(i);
				os << "\t";
				dotNode(os, t);
				os << edge;
				dotNode(os, h);
				dotWeight(os, i);
				os << ";" << std::endl;
			}
		}

		os << "}" << std::endl;
	}
};

static void graph_del(void *p) {
	DsliteGraph *dg = (DsliteGraph *) p;
	delete dg;
}

struct agg_ctxt_t {
	void *p;
};

static DsliteGraph* get_or_create_graph(sqlite3_context *ctxt, const char *name, const char *gspec)
{
	agg_ctxt_t *pagg = (agg_ctxt_t *) sqlite3_aggregate_context(ctxt, sizeof(agg_ctxt_t)); 
	if (!pagg->p && name) {
		if (!dslite::wormhole_get(name, &pagg->p)) {
			if (!DsliteGraph::isValidGraphSpec(gspec)) {
				return 0;
			}
			pagg->p = (void *) new DsliteGraph (gspec);
			dslite::wormhole_add(name, pagg->p, graph_del);
		}
	}
	return (DsliteGraph *) pagg->p;
}

static void graph_step(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char *name = (const char*) sqlite3_value_text(argv[0]);
	const char *gs = (const char*) sqlite3_value_text(argv[1]);

	if (DsliteGraph::isNotWeighted(gs)) {
		DS_CHECK_COND_RESULT(argc == 4, ctxt, "bad param to unweighted graph");
	} else { 
		DS_CHECK_COND_RESULT(argc == 5, ctxt, "bad param to weighted graph");
	}

	DsliteGraph *g = get_or_create_graph(ctxt, name, gs);
	DS_CHECK_COND_RESULT(g, ctxt, "cannot find or create graph"); 

	int arcidx = g->addArc(argc-2, &argv[2]);
	DS_CHECK_COND_RESULT(arcidx >= 0, ctxt, "invalid edge");
}

static void graph_final(sqlite3_context *ctxt)
{
	DsliteGraph *g = get_or_create_graph(ctxt, 0, 0); 
	if (!g) {
		sqlite3_result_error(ctxt, "cannot find graph", -1);
	}
	int rc = g->build();
	DS_CHECK_COND_RESULT(rc >= 0, ctxt, "invalid edge");
	sqlite3_result_int(ctxt, 1);
}

struct gCursor {
	sqlite3_vtab_cursor base;

	DsliteGraph *g;
	bool node;
	sqlite3_int64 iRowid;
};

static DsliteGraph* get_graph(const char *name)
{
	void *p = 0;
	if (!dslite::wormhole_get(name, &p)) {
		return 0;
	}
	return (DsliteGraph *) p;
}

static int gConnect(
		sqlite3 *db, void *pAux,
		int argc, const char *const* argv,
		sqlite3_vtab **ppt, 
		char **pzErr) 
{
	int rc;
	DsliteGraph *g = 0;

	DS_CHECK_COND(argc == 5, pzErr, "graph_unnest(name, node|edge)");
	g = get_graph(argv[3]);
	DS_CHECK_COND(g, pzErr, "cannot find group");

	std::string node("node");
	std::string edge("edge");

	const char* schema = 0;

	if (node == argv[4]) {
		if (g->needMapNode()) { 
			schema = "CREATE TABLE x(id int, tag text)";
		} else {
			schema = "CREATE TABLE x(id int)";
		}
	} else if (edge == argv[4]) {
		if (g->i64Weighted()) {
			schema = "CREATE TABLE x(tail int, head int, weight bigint)";
		} else if (g->f64Weighted()) {
			schema = "CREATE TABLE x(tail int, head int, weight double)";
		} else {
			schema = "CREATE TABLE x(tail int, head int)"; 
		}
	} else {
		DS_CHECK_COND(false, pzErr, "graph_unnest(name, node|edge)");
	}

	rc = sqlite3_declare_vtab(db, schema);
	DS_CHECK_COND(rc == SQLITE_OK, pzErr, "cannot declare graph unest schema.");

	*ppt = (sqlite3_vtab *) sqlite3_malloc(sizeof(**ppt));
	DS_CHECK_MEM(*ppt);
	::memset(*ppt, 0, sizeof(**ppt)); 
	return SQLITE_OK;
}

static int gDisconnect(sqlite3_vtab *pt)
{
	sqlite3_free(pt);
	return SQLITE_OK;
}

static int gOpen(sqlite3_vtab *p, sqlite3_vtab_cursor **ppCur)
{
	gCursor *pCur = (gCursor *) sqlite3_malloc(sizeof(*pCur));
	DS_CHECK_MEM(pCur);
	::memset(pCur, 0, sizeof(*pCur));
	*ppCur = &pCur->base;
	return SQLITE_OK;
}

static int gClose(sqlite3_vtab_cursor *cur)
{
	sqlite3_free(cur);
	return SQLITE_OK;
}

static int gNext(sqlite3_vtab_cursor *cur)
{
	gCursor *pCur = (gCursor *) cur;
	if (pCur->iRowid >= 0) {
		if (pCur->node) {
			if (pCur->iRowid >= pCur->g->num_nodes()) {
				pCur->iRowid = -1;
			} else {
				pCur->iRowid++;
			}
		} else {
			if (pCur->iRowid >= pCur->g->num_arcs()) {
				pCur->iRowid = -1;
			} else {
				pCur->iRowid++;
			}
		}
	}
	return SQLITE_OK;
}

static int gColumn(sqlite3_vtab_cursor *cur, sqlite3_context *ctxt, int i)
{
	gCursor *pCur = (gCursor *) cur;
	if (pCur->node) {
		if (i == 0) {
			sqlite3_result_int(ctxt, pCur->iRowid - 1);
		} else {
			const std::string& s = pCur->g->i2s[pCur->iRowid - 1];
			sqlite3_result_text(ctxt, s.c_str(), -1, SQLITE_STATIC);
		}
	} else {
		if (i == 0) {
			sqlite3_result_int(ctxt, pCur->g->Tail(pCur->iRowid - 1));
		} else if (i == 1) {
			sqlite3_result_int(ctxt, pCur->g->Head(pCur->iRowid - 1));
		} else {
			if (pCur->g->i64Weighted()) {
				sqlite3_result_int64(ctxt, pCur->g->iWeight(pCur->iRowid - 1));
			} else {
				sqlite3_result_double(ctxt, pCur->g->fWeight(pCur->iRowid - 1));
			}
		}
	}
	return SQLITE_OK;
}

static int gRowid(sqlite3_vtab_cursor *cur, sqlite_int64 *pRowid)
{
	gCursor *pCur = (gCursor *) cur;
	*pRowid = pCur->iRowid;
	return SQLITE_OK;
}

static int gEof(sqlite3_vtab_cursor *cur) {
	gCursor *pCur = (gCursor *) cur;
	return pCur->iRowid < 0;
}

static int gFilter(
		sqlite3_vtab_cursor *cur, int idxNum, const char *idxStr,
		int argc, sqlite3_value **argv
		)
{
	gCursor *pCur = (gCursor *) cur;
	if (!pCur->g) {
		const char* name = (const char *) sqlite3_value_text(argv[0]);
		pCur->g = get_graph(name); 
		if (!pCur->g) {
			return SQLITE_ERROR;
		}
	}

	const std::string node("node");
	const char *v = (const char*) sqlite3_value_text(argv[1]);
	if (node == v) {
		pCur->node = true;
	} 
	pCur->iRowid = 0;
	return gNext(cur);
}
	
static int gBestIndex(sqlite3_vtab *tab, sqlite3_index_info *pidxinfo)
{
	pidxinfo->estimatedCost = 1000000;
	return SQLITE_OK;
}

static sqlite3_module unnest_graph = {
	0,					/* iVersion */
	0,					/* xCreate, eponymous */
	gConnect,	
	gBestIndex,	
	gDisconnect,
	0,					/* xDestroy, same as Disconnect */
	gOpen, 
	gClose,
	gFilter,
	gNext,
	gEof,
	gColumn,
	gRowid,
	0,					// xUpdate
	0,					// xBegin 
	0,					// xSync 
	0,					// xCommit
	0,					// xRollback
	0,					// xFindMethod
	0,					// xRename
};

static void graph_dot(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char *name = (const char*) sqlite3_value_text(argv[0]);
	DsliteGraph *g = get_graph(name);
	if(!g) {
		sqlite3_result_error(ctxt, "cannot find graph", -1);
	}

	std::stringstream ss;
	g->dot(ss);
	std::string str = ss.str();
	sqlite3_result_text(ctxt, str.data(), str.size(), SQLITE_TRANSIENT);
}

extern int graph_init(sqlite3 *db, char **pzErrMsg);
int graph_init(sqlite3 *db, char **pzErr)
{
	int rc;

	rc = sqlite3_create_function(db, "dslite_graph", -1, SQLITE_UTF8, 0, 
			0, graph_step, graph_final); 
	DS_CHECK_COND(rc == SQLITE_OK, pzErr, "cannot create function dslite_graph");

	rc = sqlite3_create_function(db, "dslite_graph_dot", 1, SQLITE_UTF8, 0,
			graph_dot, 0, 0);
	DS_CHECK_COND(rc == SQLITE_OK, pzErr, "cannot create function dslite_graph_dot");
	
	rc = sqlite3_create_module(db, "dslite_graph_unnest", &unnest_graph, 0);
	DS_CHECK_COND(rc == SQLITE_OK, pzErr, "cannot create function dslite_graph_unnest");

	return SQLITE_OK;
}

extern "C"
int sqlite3_dslitegraph_init(
		sqlite3 *db,
		char **pzErrMsg,
		const sqlite3_api_routines *pApi
		)
{
	int rc;
	SQLITE_EXTENSION_INIT2(pApi);

	if ((rc = graph_init(db, pzErrMsg)) != SQLITE_OK) {
		return rc;
	}
	return rc;
}

