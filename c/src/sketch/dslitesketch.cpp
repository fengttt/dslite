#include "sqlite3ext.h"
SQLITE_EXTENSION_INIT1

#include "../util/ds_sqlite.h"

#include "../util/wormhole.hpp"
#include <cassert>
#include <cstring>

using namespace std;
using namespace dslite;

#include "sketch.cpp"
#include "window.cpp"



extern int sketch_init(sqlite3 *db, char **pzErrMsg);
extern int window_init(sqlite3 *db, char **pzErrMsg);

extern "C"
int sqlite3_dslitesketch_init(
		sqlite3 *db,
		char **pzErrMsg,
		const sqlite3_api_routines *pApi
		)
{
	int rc;
	SQLITE_EXTENSION_INIT2(pApi);

	if ((rc = sketch_init(db, pzErrMsg)) != SQLITE_OK) {
		return rc;
	}

	if ((rc = window_init(db, pzErrMsg)) != SQLITE_OK) {
		return rc;
	}

	return rc;
}

