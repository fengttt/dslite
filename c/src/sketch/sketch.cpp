#include "hll/include/hll.hpp"
#include "kll/include/kll_sketch.hpp"

static void hll_delete(void *p) {
	datasketches::hll_sketch *phll = (datasketches::hll_sketch *) p;
	delete phll;
}

struct agg_ctxt_t {
	void *p;
};

static datasketches::hll_sketch *get_or_create_hll(sqlite3_context *ctxt, const char *hllname)
{
	agg_ctxt_t *pagg = (agg_ctxt_t *) sqlite3_aggregate_context(ctxt, sizeof(agg_ctxt_t)); 
	if (!pagg->p && hllname) {
		if (!wormhole_get(hllname, &pagg->p)) { 
			// lg_k should be from 6-21, 12 or 14 are good default
			// hll_type default to HLL4
			pagg->p = (void *) new datasketches::hll_sketch(14); 
			if (pagg->p) { 
				wormhole_add(hllname, pagg->p, hll_delete);
			}
		}
	}
	return (datasketches::hll_sketch *) pagg->p; 
}


static void hll_step(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* hllname = (const char *) sqlite3_value_text(argv[0]); 
	datasketches::hll_sketch *phll = get_or_create_hll(ctxt, hllname);
	if (!phll) {
		sqlite3_result_error(ctxt, "cannot find or create hll", -1);
	}
	const char* v = (const char *) sqlite3_value_text(argv[1]); 
	phll->update(std::string(v));
}

static void hll_final(sqlite3_context *ctxt)
{
	datasketches::hll_sketch *phll = get_or_create_hll(ctxt, 0); 
	if (!phll) {
		sqlite3_result_error(ctxt, "cannot find or create hll", -1);
	}
	double est = phll->get_estimate();
	sqlite3_result_double(ctxt, est);
}

static void hll_estimate(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* hllname = (const char *) sqlite3_value_text(argv[0]); 
	datasketches::hll_sketch *phll = 0; 
	wormhole_get(hllname, (void **) &phll); 
	if (!phll) {
		sqlite3_result_error(ctxt, "cannot find hll", -1);
	}

	double ret = 0;

	if (argc == 1) {
		ret = phll->get_estimate();
	} else {
		int nstdev = sqlite3_value_int(argv[1]);
		if (nstdev < 0) {
			ret = phll->get_lower_bound((unsigned) -nstdev); 
		} else { 
			ret = phll->get_upper_bound((unsigned) nstdev); 
		} 
	}

	sqlite3_result_double(ctxt, ret); 
}

typedef datasketches::kll_sketch<float> kll_float_sketch;
typedef datasketches::kll_sketch<std::string> kll_string_sketch;
		
static void klls_delete(void *p) {
	kll_string_sketch *pkll = (kll_string_sketch *) p;
	delete pkll;
}

static void kllf_delete(void *p) {
	kll_float_sketch *pkll = (kll_float_sketch *) p;
	delete pkll;
}

static void *get_or_create_kll(sqlite3_context *ctxt, const char *name, bool str)
{
	agg_ctxt_t *pagg = (agg_ctxt_t *) sqlite3_aggregate_context(ctxt, sizeof(agg_ctxt_t)); 
	if (!pagg->p && name) {
		if (!wormhole_get(name, &pagg->p)) { 
			if (str) {
				pagg->p = (void *) new kll_string_sketch(); 
				wormhole_add(name, pagg->p, klls_delete);
			} else {
				pagg->p = (void *) new kll_float_sketch(); 
				wormhole_add(name, pagg->p, kllf_delete);
			}
		}
	}
	return pagg->p;
}

static void *get_kll_by_name(const char *name)
{
	void *p = 0;
	wormhole_get(name, &p); 
	return p;
}

static void strkll_step(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* name = (const char *) sqlite3_value_text(argv[0]); 
	kll_string_sketch *pkll = (kll_string_sketch *) get_or_create_kll(ctxt, name, true);

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}
	const char* v = (const char *) sqlite3_value_text(argv[1]); 
	pkll->update(std::string(v));
}

static void strkll_final(sqlite3_context *ctxt) 
{
	kll_string_sketch *pkll = (kll_string_sketch *) get_or_create_kll(ctxt, 0, true);

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	int64_t n = (int64_t) pkll->get_n();
	sqlite3_result_int64(ctxt, n); 
}

static void fltkll_step(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* name = (const char *) sqlite3_value_text(argv[0]); 
	kll_float_sketch *pkll = (kll_float_sketch *) get_or_create_kll(ctxt, name, false);

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	float v = (float) sqlite3_value_double(argv[1]);
	pkll->update(v); 
}

static void fltkll_final(sqlite3_context *ctxt)
{
	kll_float_sketch *pkll = (kll_float_sketch *) get_or_create_kll(ctxt, 0, false); 

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	int64_t n = pkll->get_n();
	sqlite3_result_int64(ctxt, n); 
}

static void strkll_n(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* name = (const char *) sqlite3_value_text(argv[0]); 
	kll_string_sketch *pkll = (kll_string_sketch *) get_kll_by_name(name); 

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	int64_t n = pkll->get_n();
	sqlite3_result_int64(ctxt, n); 
}

static void fltkll_n(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* name = (const char *) sqlite3_value_text(argv[0]); 
	kll_float_sketch *pkll = (kll_float_sketch *) get_kll_by_name(name); 

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	int64_t n = pkll->get_n();
	sqlite3_result_int64(ctxt, n); 
}

static void strkll_quantile(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* name = (const char *) sqlite3_value_text(argv[0]); 
	kll_string_sketch *pkll = (kll_string_sketch *) get_kll_by_name(name); 

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	float rank = sqlite3_value_double(argv[1]);
	std::string s = pkll->get_quantile(rank);
	sqlite3_result_text(ctxt, s.c_str(), -1, SQLITE_TRANSIENT); 
}

static void fltkll_quantile(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* name = (const char *) sqlite3_value_text(argv[0]); 
	kll_float_sketch *pkll = (kll_float_sketch *) get_kll_by_name(name); 

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	float rank = sqlite3_value_double(argv[1]);
	float q = pkll->get_quantile(rank);
	sqlite3_result_double(ctxt, q); 
}

static void strkll_rank(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* name = (const char *) sqlite3_value_text(argv[0]); 
	kll_string_sketch *pkll = (kll_string_sketch *) get_kll_by_name(name); 

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	const char* s = (const char *) sqlite3_value_text(argv[1]);
	float rank = pkll->get_rank(std::string(s)); 
	sqlite3_result_double(ctxt, rank); 
}

static void fltkll_rank(sqlite3_context *ctxt, int argc, sqlite3_value **argv)
{
	const char* name = (const char *) sqlite3_value_text(argv[0]); 
	kll_float_sketch *pkll = (kll_float_sketch *) get_kll_by_name(name); 

	if (!pkll) {
		sqlite3_result_error(ctxt, "cannot find or create kll", -1);
	}

	float f = sqlite3_value_double(argv[1]); 
	float rank = pkll->get_rank(f); 
	sqlite3_result_double(ctxt, rank); 
}

extern int sketch_init(sqlite3 *db, char **pzErrMsg);
int sketch_init(sqlite3 *db, char **pzErrMsg)
{
	int rc;

	rc = sqlite3_create_function(db, "dslite_hll", 2, SQLITE_UTF8, 0, 
			0, hll_step, hll_final); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_hll_estimate", 1, SQLITE_UTF8, 0, 
			hll_estimate, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_hll_bound", 2, SQLITE_UTF8, 0, 
			hll_estimate, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_stringkll", 2, SQLITE_UTF8, 0, 
			0, strkll_step, strkll_final); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_floatkll", 2, SQLITE_UTF8, 0, 
			0, fltkll_step, fltkll_final); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_stringkll_n", 1, SQLITE_UTF8, 0, 
			strkll_n, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_floatkll_n", 1, SQLITE_UTF8, 0, 
			fltkll_n, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_stringkll_quantile", 2, SQLITE_UTF8, 0, 
			strkll_quantile, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_floatkll_quantile", 2, SQLITE_UTF8, 0, 
			fltkll_quantile, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_stringkll_rank", 2, SQLITE_UTF8, 0, 
			strkll_rank, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "dslite_floatkll_rank", 2, SQLITE_UTF8, 0, 
			fltkll_rank, 0, 0); 
	return rc;
}

