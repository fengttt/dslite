#include "../util/ds_window.hpp"

static void win_del(void *p) {
	Window *pw = (Window *) p;
	delete pw;
}

static Window *get_or_create_window(sqlite3_context *ctxt, int argc, sqlite3_value **argv, int wt) 
{
	void *vp = sqlite3_get_auxdata(ctxt, 0); 
	if (!vp) {
		const char* name = (const char*) sqlite3_value_text(argv[0]); 
		const char* coltypes = (const char*) sqlite3_value_text(argv[1]);
		if (!wormhole_get(name, &vp)) {
			if (wt == WINDOW_N || wt == WINDOW_RESERVOIR) {
				int n = sqlite3_value_int(argv[2]);
				if (n <= 0) {
					return 0;
				}
				vp = (void *) new Window(coltypes, wt, n, 0); 
			} else if (wt == WINDOW_TS) {
				double ts = sqlite3_value_double(argv[2]);
				if (ts <= 0) {
					return 0;
				}
				vp = (void *) new Window(coltypes, wt, 0, ts); 
			}

			if (!vp) {
				return 0;
			}
			wormhole_add(name, vp, win_del);
		}
		sqlite3_set_auxdata(ctxt, 0, vp, 0);
	}

	return (Window *) vp; 
}

static void window_impl(sqlite3_context *ctxt, int argc, sqlite3_value **argv, int wt) 
{
	if (argc <= 3) {
		sqlite3_result_error(ctxt, "window function takes at least 4 args", -1);
	}

	Window *win = get_or_create_window(ctxt, argc, argv, wt); 
	if (!win || win->cts_ncol() + 3 != argc) {
		sqlite3_result_error(ctxt, "window not found or args mismatch", -1);
	}
	int64_t cnt = win->add_tuple(argv+3);
	sqlite3_result_int64(ctxt, cnt);
}

static void window_n(sqlite3_context *ctxt, int argc, sqlite3_value **argv) 
{
	window_impl(ctxt, argc, argv, WINDOW_N);
}

static void window_ts(sqlite3_context *ctxt, int argc, sqlite3_value **argv) 
{
	window_impl(ctxt, argc, argv, WINDOW_TS);
}

static void window_reservoir(sqlite3_context *ctxt, int argc, sqlite3_value **argv) 
{
	window_impl(ctxt, argc, argv, WINDOW_RESERVOIR); 
}

struct unnestTab {
	sqlite3_vtab base;
	std::string *coltypes;
};

struct unnestCursor {
	sqlite3_vtab_cursor base;
	std::vector<const dslite::Tuple*> *vec;
	int64_t winid;
	Window *win;
	WindowUnnestFunction *wf;
	sqlite3_int64 iRowid;
};

static int unnestConnect(
		sqlite3 *db, void *pAux, 
		int argc, const char *const* argv,
		sqlite3_vtab **ppt,
		char **pzErr)
{
	int rc = SQLITE_OK;
	DS_CHECK_COND(argc == 4, pzErr, "unnest table does not have schema info.");

	std::string coltypes(argv[3]);
	sqlite3_str *pStr = sqlite3_str_new(0);
	sqlite3_str_appendf(pStr, "CREATE TABLE x(");
	for (int ii = 0; ii < (int) coltypes.size(); ii++) {
		const char *ct = dslite::colt2sqlt(coltypes[ii]); 
		sqlite3_str_appendf(pStr, "c%d %s,", ii, ct); 
	}
	sqlite3_str_appendf(pStr, "wormhole hidden, winid hidden, unnestfunc hidden)"); 
	char* pSchema = (char*) sqlite3_str_finish(pStr);
	rc = sqlite3_declare_vtab(db, pSchema);
	sqlite3_free(pSchema);
	DS_CHECK_COND(rc == SQLITE_OK, pzErr, "cannot declare unnest table schema");
	
	unnestTab *tab = (unnestTab *) sqlite3_malloc(sizeof(unnestTab));
	*ppt = (sqlite3_vtab *) tab;
	DS_CHECK_MEM(*ppt);
	::memset(*ppt, 0, sizeof(**ppt)); 

	tab->coltypes = new std::string(coltypes);
	return SQLITE_OK;
}

/* 
 * prevent unnest as eponymous virtual table -- we need to fingure out 
 * schema from argv.
 */
static int unnestCreate(
		sqlite3 *db, void *pAux, 
		int argc, const char *const* argv,
		sqlite3_vtab **ppt,
		char **pzErr)
{
	return unnestConnect(db, pAux, argc, argv, ppt, pzErr);
}

static int unnestDisconnect(sqlite3_vtab *pt)
{
	unnestTab *pTab = (unnestTab *) pt;
	if (pTab->coltypes) {
		delete pTab->coltypes;
		pTab->coltypes = 0;
	}
	sqlite3_free(pt);
	return SQLITE_OK;
}

static int unnestOpen(sqlite3_vtab *p, sqlite3_vtab_cursor **ppCur)
{
	unnestCursor *pCur = (unnestCursor *) sqlite3_malloc(sizeof(*pCur));
	if (pCur == 0) {
		return SQLITE_NOMEM;
	}
	::memset(pCur, 0, sizeof(*pCur));
	*ppCur = &pCur->base;
	return SQLITE_OK;
}

static int unnestClose(sqlite3_vtab_cursor *cur)
{
	unnestCursor *pCur = (unnestCursor *) cur;
	if (pCur->wf) {
		delete pCur->wf;
	}

	sqlite3_free(pCur);
	return SQLITE_OK;
}

static int unnestNext(sqlite3_vtab_cursor *cur)
{
	unnestCursor *pCur = (unnestCursor *) cur;
	if (pCur->iRowid >= 0) {
		if (pCur->iRowid >= (int64_t) pCur->vec->size()) {
			pCur->iRowid = -1;
		} else {
			pCur->iRowid++;
		}
	}
	return SQLITE_OK;
}

static int unnestColumn(sqlite3_vtab_cursor *cur, sqlite3_context *ctxt, int i)
{
	unnestCursor *pCur = (unnestCursor *) cur;
	unnestTab *pTab = (unnestTab *) pCur->base.pVtab;
	int nc = (int) pTab->coltypes->size();

	if (i == nc || i == nc+2) {
		// Window name, unnest function name, NYI
		sqlite3_result_null(ctxt);
		return SQLITE_OK;
	} else if (i == nc+1) {
		sqlite3_result_int64(ctxt, pCur->winid);
		return SQLITE_OK;
	} 

	dslite::Tuple *tup = (dslite::Tuple *) (*(pCur->vec))[pCur->iRowid - 1];
	int err = tup->get_col(*pTab->coltypes, i, ctxt);
	if (err < 0) {
		sqlite3_result_error(ctxt, "cannot retrive window data", -1);
		return SQLITE_ERROR;
	}
	return SQLITE_OK;
}

static int unnestRowid(sqlite3_vtab_cursor *cur, sqlite_int64 *pRowid) 
{
	unnestCursor *pCur = (unnestCursor *) cur;
	*pRowid = pCur->iRowid;
	return SQLITE_OK;
}

static int unnestEof(sqlite3_vtab_cursor *cur) {
	unnestCursor *pCur = (unnestCursor *) cur;
	return pCur->iRowid < 0;
}

static int unnestFilter(
		sqlite3_vtab_cursor *cur, 
		int idxNum, const char *idxStr,
		int argc, sqlite3_value **argv
		)
{
	unnestCursor *pCur = (unnestCursor *) cur;

	int i = 0;
	const char *name = 0;
	const char *func = "unnest_default";

	if (argc == 0) {
		return SQLITE_ERROR;
	}

	name = (const char *) sqlite3_value_text(argv[i++]);
	if (!name) {
		return SQLITE_ERROR;
	}
	pCur->winid = sqlite3_value_int64(argv[i++]);

	if (argc == 3) {
		func = (const char *) sqlite3_value_text(argv[i++]);
	} 

	if (!pCur->win) {
		if (!wormhole_get(name, (void **) &pCur->win)) {
			return SQLITE_ERROR;
		}
	}

	if (!pCur->wf) {
		pCur->wf = dslite::create_window_unnest_function(func); 
		if (!pCur->wf) {
			return SQLITE_ERROR;
		}
	}

	pCur->vec = pCur->wf->run(pCur->win);
	pCur->iRowid = 0;
	return unnestNext(cur);
}

#define PC (pidxinfo->aConstraint)
#define PCI(i) ((PC) + i)
static int unnestBestIndex(sqlite3_vtab *tab, sqlite3_index_info *pidxinfo)
{
	unnestTab *ut = (unnestTab *) tab;
	int WORMHOLE_COL = (int) ut->coltypes->size(); 
	int WINID_COL = WORMHOLE_COL + 1; 
	int UFUNC_COL = WINID_COL + 1; 
	int idxNum = 0;
	int aIdx[3];

	aIdx[0] = aIdx[1] = aIdx[2] = -1;
	for (int i = 0; i < pidxinfo->nConstraint; i++) { 
		if (PCI(i)->op == SQLITE_INDEX_CONSTRAINT_EQ) {
			if (PCI(i)->usable == 0) {
				continue;
			}

			if (PCI(i)->iColumn == WORMHOLE_COL) { 
				if (aIdx[0] > 0) {
					return SQLITE_CONSTRAINT;
				}
				idxNum |= 1;
				aIdx[0] = i;
			} else if (PCI(i)->iColumn == WINID_COL) {
				if (aIdx[0] > 0) {
					return SQLITE_CONSTRAINT;
				}
				idxNum |= 2;
				aIdx[1] = i;
			} else if (PCI(i)->iColumn == UFUNC_COL) {
				if (aIdx[0] > 0) {
					return SQLITE_CONSTRAINT;
				}
				idxNum |= 4;
				aIdx[2] = i;
			}
		}
	}

	/* wormhole and winid are required. */
	if (aIdx[0] < 0 || aIdx[1] < 0) {
		return SQLITE_CONSTRAINT;
	}
	pidxinfo->idxNum = idxNum;

	int nArg = 0;
	for (int i = 0; i < 3; i++) {
		if (aIdx[i] >= 0) {
			pidxinfo->aConstraintUsage[aIdx[i]].argvIndex = ++nArg;
			pidxinfo->aConstraintUsage[aIdx[i]].omit = 1;
		}
	}
	/* Wild guess. */
	pidxinfo->estimatedRows = 10000;
	return SQLITE_OK;
}

static sqlite3_module unnest_module = {
	0,					/* iVersion */
	unnestCreate,		
	unnestConnect,	
	unnestBestIndex,	
	unnestDisconnect,
	unnestDisconnect,	/* xDestroy, same as Disconnect */
	unnestOpen, 
	unnestClose,
	unnestFilter,
	unnestNext,
	unnestEof,
	unnestColumn,
	unnestRowid,
	0,					// xUpdate
	0,					// xBegin 
	0,					// xSync 
	0,					// xCommit
	0,					// xRollback
	0,					// xFindMethod
	0,					// xRename
};


extern int window_init(sqlite3 *db, char **pzErrMsg);
int window_init(sqlite3 *db, char **pzErrMsg)
{
	int rc;
	rc = sqlite3_create_module(db, "dslite_window_unnest", &unnest_module, 0);
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "window_n", -1, SQLITE_UTF8, 0, window_n, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "window_ts", -1, SQLITE_UTF8, 0, window_ts, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	rc = sqlite3_create_function(db, "window_reservoir", -1, SQLITE_UTF8, 0, window_reservoir, 0, 0); 
	if (rc != SQLITE_OK) {
		return rc;
	}

	return rc;
}

