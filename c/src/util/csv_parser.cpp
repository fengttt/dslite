#include "csv_parser.hpp"
#include "ds_pipe.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <cstring>

#include "zlib.h"

using namespace dslite;

CsvParser::CsvParser(int ncol) :
	m_readfn(0), m_readfn_hndl(0),
	m_ncol(ncol), m_csvmode(true), 
	m_hdrline(false), 
	m_delim(','), m_quote('"'), m_escape('"'), m_nullstr_ptr(0),
	m_rbuf_end(0),
	m_currline_begin(0), m_currline_end(0),
	m_nextline_begin(0), m_eat_next_lf(0),
	m_currline_no(0), m_nrow(0)
{
	if (m_ncol <= 0) {
		m_ncol = 1;
	}

	m_errmsg[0] = 0;
}

bool CsvParser::start_internal(int (*readfn)(void *hndl, void *buf, int bufsz, char *errmsg, int errsz),
			void *hndl)
{
	m_readfn = readfn;
	m_readfn_hndl = hndl;

	m_eat_next_lf = false;
	m_currline_no = 0;

	m_fields.reserve(m_ncol + 1);
	m_rbuf.resize(BUFSZ + SSESZ);

	char sse_buf[SSESZ] = {0};
	sse_buf[0] = m_delim;
	if (m_csvmode) {
		if (m_quote != 0) {
			if (m_escape != m_quote || m_escape == 0) {
				snprintf(m_errmsg, ERRMSGSZ, "Bad csv options: escape %c != quote %c", 
						m_escape == 0 ? '0' : m_escape, m_quote);
				return false;
			}
			sse_buf[1] = m_quote;
		} else if (m_escape != 0) {
			sse_buf[1] = m_escape;
		}
	} else {
		if (m_escape != '\\') {
			snprintf(m_errmsg, ERRMSGSZ, "Bad pgtext options: escape %c", 
					m_escape == 0 ? '0' : m_escape); 
			return false;
		}
		sse_buf[1] = m_escape;
	}

#ifdef USE_SSE 
	m_xmm_sep_search = _mm_loadu_si128((__m128i*) sse_buf);
#else
	m_xmm_sep_search[0] = sse_buf[0];
	m_xmm_sep_search[1] = sse_buf[1];
#endif

	sse_buf[0] = '\r';
	sse_buf[1] = '\n';
	sse_buf[2] = m_quote; 

#ifdef USE_SSE 
	m_xmm_line_search = _mm_loadu_si128((__m128i*) sse_buf);
#else
	m_xmm_line_search[0] = sse_buf[0];
	m_xmm_line_search[1] = sse_buf[1];
	m_xmm_line_search[2] = sse_buf[2];
#endif

	if (!load_buf()) {
		return false;
	}

	// discard hdrline.
	if (m_hdrline) {
		bool ok = next_line();
		if (!ok) {
			return false;
		}
	}
	return true;
}

bool CsvParser::load_buf()
{
	int64_t rsz = m_readfn(m_readfn_hndl, &m_rbuf[0], BUFSZ, m_errmsg, ERRMSGSZ);
	if (rsz < 0) {
		return false;
	}

	m_rbuf_end = &m_rbuf[0] + rsz;
	memset(m_rbuf_end, 0, SSESZ);
	m_currline_begin = m_currline_end = 0; 
	m_nextline_begin = rsz == 0 ? 0 : &m_rbuf[0];
	return 1;
}

bool CsvParser::next_line_raw()
{
	// next line exhausted rbuf (prev rbuf ends with \n)
	if (m_nextline_begin == m_rbuf_end) {
		if (!load_buf()) {
			return false;
		}
	}

	if (m_nextline_begin == 0) {
		return false;
	}

	bool inquote = false;
	if (m_quote == 0) {
		return advance_line<false>(false, &inquote);
	} else {
		return advance_line<true>(false, &inquote);
	}
}

static inline uint16_t MASK_OFF_BIT(uint16_t bits, int n)
{
	static const uint16_t masks[16] = {
		1 << 0, 1 << 1, 1 << 2, 1 << 3, 
		1 << 4, 1 << 5, 1 << 6, 1 << 7, 
		1 << 8, 1 << 9, 1 << 10, 1 << 11, 
		1 << 12, 1 << 13, 1 << 14, 1 << 15
	};
	return bits & (~masks[n]);
}

template<bool handle_quote>
bool CsvParser::advance_line(bool spanrbuf, bool *inquote)
{
	m_currline_begin = m_nextline_begin;
	m_nextline_begin = 0;
	char *p = 0;
	
	if (m_eat_next_lf && m_currline_begin[0] == '\n') {
		m_currline_begin += 1;
		m_eat_next_lf = false;
	}

#ifdef USE_SSE 
	__m128i xmm_buf;
	__m128i xmm_mask;

	for (p = m_currline_begin; p < m_rbuf_end; p += SSESZ) {
		xmm_buf = _mm_loadu_si128((__m128i*) p);
		xmm_mask = _mm_cmpistrm(m_xmm_line_search, xmm_buf, _SIDD_CMP_EQUAL_ANY | _SIDD_UBYTE_OPS);

		// Test if input data contains an 0x00.  0x00 is not a valid UTF-8 encoding (at least
		// in postgres sense) so we will error out here.
		//
		// We issue SSE instruction again, but GCC will optimimze it away and only check ZFlag
		// register.
		int zero = _mm_cmpistrz(m_xmm_line_search, xmm_buf, _SIDD_CMP_EQUAL_ANY | _SIDD_UBYTE_OPS);
		if (zero) {
			for (char *q = p; q < m_rbuf_end; q++) {
				if (*q == 0) {
					snprintf(m_errmsg, ERRMSGSZ, "File contains invalid char 0x00 at line %d.", m_currline_no);
					return false;
				}
			}
		}

		uint16_t xmm_mask16 = _mm_extract_epi16(xmm_mask, 0);
		while (xmm_mask16 != 0) {
			int n = __builtin_ffs(xmm_mask16) - 1;
			xmm_mask16 = MASK_OFF_BIT(xmm_mask16, n);
			char *q = p + n;
			if (handle_quote) {
				if (*q == m_quote) {
					*inquote = !(*inquote);
					continue;
				}
				if (*inquote) {
					continue;
				}
			}

			m_currline_end = q;
			if (q[0] == '\n') {
				m_nextline_begin = q+1;
			} else {
				if (q[1] == '\n') {
					m_eat_next_lf = false;
					m_nextline_begin = q+2;
				} else {
					m_eat_next_lf = true;
					m_nextline_begin = q+1;
				}
			}

			if (spanrbuf) {
				m_currline_copy.append(m_currline_begin, m_currline_end - m_currline_begin);
				m_currline_begin = &m_currline_copy[0];
				m_currline_end = m_currline_begin + m_currline_copy.size();
			}
			return parseline();
		}
	}
#else 
	for (p = m_currline_begin; p < m_rbuf_end; p++) {
		if (handle_quote) {
			if (*p == m_quote) {
				*inquote = !(*inquote);
				continue;
			}
			if (*inquote) {
				continue;
			}
		}
		if (*p == '\n') {
			m_currline_end = p;
			m_nextline_begin = p+1;
		} else if (*p == '\r') {
			m_currline_end = p;
			if (p[1] == '\n') {
				m_eat_next_lf = false;
				m_nextline_begin = p+2;
			} else {
				m_eat_next_lf = true;
				m_nextline_begin = p+1;
			}
		} else {
			continue;
		}

		if (spanrbuf) {
			m_currline_copy.append(m_currline_begin, m_currline_end - m_currline_begin);
			m_currline_begin = &m_currline_copy[0];
			m_currline_end = m_currline_begin + m_currline_copy.size();
		}
		return parseline();
	}
#endif /* USE_SSE */

	if (!spanrbuf) {
		m_currline_copy.resize(0);
	}
	m_currline_copy.append(m_currline_begin, m_rbuf_end - m_currline_begin);

	if (!load_buf()) {
		return false;
	}

	if (m_nextline_begin == 0) {
		// fake an line end, make sure has this extra char for in situ line parser.
		m_currline_copy.append("\n");
		m_currline_begin = &m_currline_copy[0];
		m_currline_end = m_currline_begin + m_currline_copy.size() - 1;
		return parseline();
	} else {
		return advance_line<handle_quote>(true, inquote);
	}
}

bool CsvParser::parseline_csv()
{
	// Parse line into fields.   Line parser is in-situ, that is we will terminate
	// each field with \0.
	bool inquote = false;
	bool currfield_quoted = false;

	m_fields.resize(0);
	m_fields.push_back(m_currline_begin);

#ifdef USE_SSE
	__m128i xmm_buf, xmm_mask;
	for (char *p = m_currline_begin; p < m_currline_end; p+=SSESZ) {
		xmm_buf = _mm_loadu_si128((__m128i*) p);
		xmm_mask = _mm_cmpistrm(m_xmm_sep_search, xmm_buf, _SIDD_CMP_EQUAL_ANY | _SIDD_UBYTE_OPS);
		uint16_t xmm_mask16 = _mm_extract_epi16(xmm_mask, 0);

		char *q;
		while (xmm_mask16 != 0) {
			int n = __builtin_ffs(xmm_mask16) - 1;
			xmm_mask16 = MASK_OFF_BIT(xmm_mask16, n);

			q = p + n;
			if (q > m_currline_end) {
				break;
			}

			if (*q == m_quote) {
				inquote = !inquote;
				currfield_quoted = true;
				continue;
			}

			if (inquote) {
				continue;
			}

			// in situ
			*q = '\0';
			if (currfield_quoted) {
				unquote_field(m_fields[m_fields.size() - 1]);
				currfield_quoted = false;
			} else {
				csv_nullprint_lastfield();
			}
			m_fields.push_back(q+1);
		}
	}
#else
	for (char *p = m_currline_begin; p < m_currline_end; p++) {
		if (*p == m_quote) {
			inquote = !inquote;
			currfield_quoted = true;
			continue;
		}
		if (inquote) {
			continue;
		}
		if (*p != m_delim) {
			continue;
		}
		
		*p = 0;
		if (currfield_quoted) {
			unquote_field(m_fields[m_fields.size() - 1]);
			currfield_quoted = false;
		} else {
			csv_nullprint_lastfield();
		}
		m_fields.push_back(p+1);
	}
#endif

	*m_currline_end = 0;
	if (currfield_quoted) {
		unquote_field(m_fields[m_fields.size() - 1]);
		currfield_quoted = false;
	} else {
		csv_nullprint_lastfield();
	}
	return true;
}

void CsvParser::unquote_field(char *p)
{
	// Assume quote and escape are the same, we drop first " char, then
	// for every "" we emit one ".
	int nq = 0;
	char *dst = p;
	while (*p) {
		if (*p == m_quote) {
			nq++;
			if (nq > 2 && (nq & 1)) {
				*dst++ = *p;
			}
		} else {
			*dst++ = *p;
		}
		p++;
	}
	*dst = 0;
}

void CsvParser::csv_nullprint_lastfield()
{
	int pos = m_fields.size() - 1;
	char *pf = m_fields[pos];

	if (pf == nullptr) {
		return;
	}

	if (!m_nullstr_ptr) {
		if (pf[0] == '\0') {
			m_fields[pos] = nullptr;
		} 
	} else {
		if (strcmp(pf, m_nullstr_ptr) == 0) {
			m_fields[pos] = nullptr;
		}
	}
}

bool CsvParser::parseline_pgtext()
{
	m_fields.resize(0);
	m_fields.push_back(m_currline_begin);
	if (m_currline_end - m_currline_begin == 2 &&
			m_currline_begin[0] == '\\' &&
			m_currline_begin[1] == '.') {
		// pgtext: \. on its own line meand end of input
		return false;
	}

	char *currescape = 0;

#ifdef USE_SSE
	__m128i xmm_buf, xmm_mask;
	for (char *p = m_currline_begin; p < m_currline_end; p+=SSESZ) {
		xmm_buf = _mm_loadu_si128((__m128i*) p);
		xmm_mask = _mm_cmpistrm(m_xmm_sep_search, xmm_buf, _SIDD_CMP_EQUAL_ANY | _SIDD_UBYTE_OPS);
		uint16_t xmm_mask16 = _mm_extract_epi16(xmm_mask, 0);
		char *q;
		while (xmm_mask16 != 0) {
			int n = __builtin_ffs(xmm_mask16) - 1;
			xmm_mask16 = MASK_OFF_BIT(xmm_mask16, n);

			q = p+n;
			if (q > m_currline_end) {
				break;
			}
			if (q == currescape + 1) {
				continue;
			}
			if (*q == '\\') {
				currescape = q;
				continue;
			}

			*q = 0;
			unescape_lastfield(currescape != 0);
			currescape = 0;
			m_fields.push_back(q+1);
		}
	}
#else 
	for (char *p = m_currline_begin; p < m_currline_end; p++) {
		if (*p == '\\') {
			currescape = p;
			continue;
		}
		if (p == currescape + 1) {
			continue;
		}

		*p = 0;
		unescape_lastfield(currescape != 0);
		currescape = 0;
		m_fields.push_back(p+1);
	}
#endif

	*m_currline_end = 0;
	unescape_lastfield(currescape != 0);
	currescape = 0;
	return true;
}

static inline int hex_to_int(char hex) {
	if (hex >= '0' && hex <= '9') {
		return hex - '0';
	} else if (hex >= 'a' && hex <= 'f') {
		return hex - 'a' + 10;
	}
	return hex - 'A' + 10;
}
static inline bool is_octal(char c) {
	return c >= '0' && c <='7';
}
static inline int oct_to_int(char c) {
	return c - '0';
}
static inline bool is_highbit_set(char c) {
	return c & 0x80;
}

void CsvParser::unescape_lastfield(bool hasescape)
{
	int pos = m_fields.size() - 1;
	char *pf = m_fields[pos];

	if (m_nullstr_ptr && strcmp(pf, m_nullstr_ptr) == 0) {
		m_fields[pos] = nullptr;
		return;
	}
	if (!hasescape) {
		return;
	}

	// postgres rule of unescape.   see copy.c CopyReadAttributesText
	bool saw_non_ascii = false;
	char *p = pf;
	char *dst = pf;
	while (*p) {
		char c = *p++;
		if (c != '\\') {
			*dst++ = c;
		} else {
			if (c == '\\') {
				c = *p++;
				switch (c) {
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
						{
							// case \013, \13, 
							int val = oct_to_int(c);
							c = *p;
							if (is_octal(c)) {
								p++;
								val = (val << 3) + oct_to_int(c);
								c = *p;
								if (is_octal(c)) {
									p++;
									val = (val << 3) + oct_to_int(c);
								}
							}
							c = val & 0377;
							if (c == 0 || is_highbit_set(val)) {
								saw_non_ascii = true;
							}
							*dst++ = c;
							break;
						}

					case 'x':
						{
							// \x3a
							c = *p;
							if (isxdigit((unsigned char) c)) {
								int val = hex_to_int(c);
								p++;
								c = *p;
								if (isxdigit((unsigned char) c)) {
									p++;
									val = (val << 4) + hex_to_int(c);
								}

								c = val & 0xff;
								if (c == 0 || is_highbit_set(val)) {
									saw_non_ascii = true;
								}
								*dst++ = c;
							} else {
								// CopyAttributeOutText quirk: \xzzz should be treated as xzzz
								*dst++ = c;
							}
							break;
						}

					case 'b': *dst++ = '\b'; break;
					case 'f': *dst++ = '\f'; break;
					case 'n': *dst++ = '\n'; break;
					case 'r': *dst++ = '\r'; break;
					case 't': *dst++ = '\t'; break;
					case 'v': *dst++ = '\v'; break;
					default: *dst++ = c; break;
				}
			}
		}
	}

	if (saw_non_ascii) {
		// Call pg_verifymbstr to check error.   Let's ignore.
	}
	*dst = '\0';
}

bool CsvParser::next(const char*** values)
{
	*values = 0;
	bool ok = next_line();
	if (!ok) {
		return false;
	}

	int line_nf = m_fields.size();

	if (!m_csvmode && line_nf > m_ncol) {
		// Too many fields: pg will treat as error, but most others are 
		// more forgiving.  Let's ignore this in csv mode. 
		snprintf(m_errmsg, ERRMSGSZ, "Too many fields at line %d.", m_currline_no);
		return false;
	}

	for (int next = line_nf; next < m_ncol; next++) {
		m_fields[next] = 0;
		if (!m_csvmode) {
			snprintf(m_errmsg, ERRMSGSZ, "Too few fields at line %d.", m_currline_no);
			return false;
		}
	}

	m_nrow++;
	*values = (const char**) &m_fields[0];
	return true;
}

// CsvFileParser handles IO, including gzip. 
static int fileparser_readfn(void *hndl, void *buf, int bufsz, char *errmsg, int errmsgsz);

class CsvFileParser : public CsvParser {
public:
	CsvFileParser(bool pipe, int ncol) 
		: CsvParser(ncol), 
		m_fd(-1), m_ispipe(pipe), m_pid(-1),
		m_gzip(false), m_gz_fetchdata(false)
	{}

	~CsvFileParser() 
	{
		if (m_gzip) {
			inflateEnd(&m_gz_stream);
		}

		if (m_ispipe) {
			if (m_pid >= 0) {
				exec_fclose(m_pid, m_pipes, false, 0, 0); 
				m_fd = -1;
			} 
		} else {
			if (m_fd != -1) {
				close(m_fd);
			}
		}
	}
		
	bool start() 
	{
		return start_internal(fileparser_readfn, (void *) this);
	}

	bool open(std::string fn)
	{
		if (m_ispipe) {
			return open_pipes(fn.c_str());
		}

		const std::string GZ = ".gz";

		if (fn.size() > GZ.size()
				&& fn.compare(fn.size() - GZ.size(), GZ.size(), GZ) == 0) {
			m_gzip = true;
			m_gz_fetchdata = true;
			m_gz_inbuf.resize(GZBUFSZ);
			m_gz_stream.zalloc = Z_NULL;
			m_gz_stream.zfree = Z_NULL;
			m_gz_stream.opaque = Z_NULL;
			m_gz_stream.total_out = 0;
			m_gz_stream.avail_in = 0;
			m_gz_stream.next_in = Z_NULL;
			if (inflateInit2(&m_gz_stream, 16+MAX_WBITS) != Z_OK) {
				snprintf(m_errmsg, ERRMSGSZ, "gzip inflateInit failed.");
				return false;
			}
		}

		m_fd = ::open(fn.c_str(), O_RDONLY);
		return m_fd != -1;
	}

	bool open_pipes(const std::string pipecmd) 
	{
		if (pipecmd == "-") {
			m_fd = STDIN_FILENO;
			return true;
		}

		m_pid = exec_fopen(pipecmd.c_str(), m_pipes);
		if (m_pid <= 0) {
			return false;
		}
		m_fd = m_pipes[1];
		return true;
	}

	int readfn(void *hndl, void *buf, int bufsz, char *errmsg, int errmsgsz)
	{
		if (m_gzip) {
			int ret;
			int64_t rsz;
			if (m_gz_fetchdata) {
				rsz = ::read(m_fd, &m_gz_inbuf[0], GZBUFSZ);
				if (rsz < 0) {
					snprintf(errmsg, errmsgsz, "Read failed: %s", strerror(errno));
					return -1;
				}
				m_gz_fetchdata = false;
				m_gz_stream.avail_in = rsz;
				m_gz_stream.next_in = (Bytef *)&m_gz_inbuf[0];
			}

			if (m_gz_stream.avail_in == 0) {
				return 0;
			}

			m_gz_stream.avail_out = bufsz;
			m_gz_stream.next_out = (Bytef *) buf;
			ret = inflate(&m_gz_stream, Z_NO_FLUSH);
			switch (ret) {
				case Z_NEED_DICT:
					snprintf(errmsg, errmsgsz, "Gzip error: need dict.");
					return -1;
				case Z_STREAM_ERROR:
					snprintf(errmsg, errmsgsz, "Gzip error: stream error.");
					return -1;
				case Z_DATA_ERROR:
					snprintf(errmsg, errmsgsz, "Gzip error: data error.");
					return -1;
				case Z_MEM_ERROR:
					snprintf(errmsg, errmsgsz, "Gzip error: mem error.");
					return -1;
				// default: OK.
			}

			if ((m_gz_stream.avail_in == 0 || m_gz_stream.avail_out != 0) && ret != Z_STREAM_END) {
				m_gz_fetchdata = true;
			}
			return bufsz - m_gz_stream.avail_out;
		} else {
			int n = ::read(m_fd, buf, bufsz);
			if (n == -1) {
				snprintf(errmsg, errmsgsz, "Read failed: %s", strerror(errno));
			}
			return n;
		}
	}

private:
	int m_fd;

	bool m_ispipe;
	int m_pid;
	int m_pipes[3];

	static const int GZBUFSZ = 2*1024*1024;
	/* gzip */
	bool m_gzip;
	z_stream m_gz_stream;
	std::vector<char> m_gz_inbuf;
	bool m_gz_fetchdata;
};


int fileparser_readfn(void *hndl, void *buf, int bufsz, char *errmsg, int errmsgsz)
{
	CsvFileParser *fp = (CsvFileParser *) hndl;
	return fp->readfn(hndl, buf, bufsz, errmsg, errmsgsz);
}

CsvParser* CsvParser::create_file_parser(const char *path, int ncol) {
	CsvFileParser *p = new CsvFileParser(false, ncol);
	if (p->open(path)) {
		return p;
	} else {
		delete p;
		return nullptr; 
	}
}

CsvParser* CsvParser::create_pipe_parser(const char *cmd, int ncol) {
	CsvFileParser *p = new CsvFileParser(true, ncol);
	if (p->open(cmd)) {
		return p;
	} else {
		delete p;
		return nullptr; 
	}
}
