#ifndef _CSV_PARSER_HPP_
#define _CSV_PARSER_HPP_

#include "ds_macros.hpp"

#include <string>
#include <vector>

#ifdef __x86_64__
#include <nmmintrin.h>
#define USE_SSE 1
#endif

namespace dslite {

class CsvParser {
	DISALLOW_COPY_AND_ASSIGN(CsvParser);

public:
	CsvParser(int ncol);
	virtual ~CsvParser() {}
	static CsvParser* create_file_parser(const char *path, int ncol);
	static CsvParser* create_pipe_parser(const char *cmd, int ncol);

	void set_csvmode(bool m) { m_csvmode = m; }
	void set_hdrline(bool h) { m_hdrline = h; }
	void set_delim(char d) { m_delim = d; }
	void set_quote(char q) { m_quote = q; }
	void set_escape(char e) { m_escape = e; }
	void set_comment(char c) { m_comment = c; }
	void set_nullstr(const std::string& s) { 
		m_nullstr = s; 
		m_nullstr_ptr = m_nullstr.c_str();
		if (s.size() == 0) {
			m_nullstr_ptr = 0;
		}
	}

	virtual bool start() = 0;
	bool next(const char ***values);

	int get_nfield() {
		return m_fields.size();
	}

	char *get_field(uint32_t n) {
		if (n >= m_fields.size()) {
			return NULL;
		}
		return m_fields[n];
	}

	const char* errmsg() { 
		if (m_errmsg[0]) {
			return m_errmsg; 
		}
		return nullptr;
	}
	bool haserror() { return errmsg() != nullptr; }

	void set_ncol(int ncol) { m_ncol = ncol; }
	int ncol() { return m_ncol; }
	int nrow() { return m_nrow; }

private:
	static const int BUFSZ = 2*1024*1024;
	static const int SSESZ = 16;		// SSE 4.2
	bool load_buf();
	bool next_line_raw();

	bool next_line() {
		while (true) {
			bool ret = next_line_raw();
			if (!ret) {
				return false;
			}

			if (m_currline_begin[0] == m_comment) {
				continue;
			} else {
				return true;
			}
		}
		return true;
	}


	template<bool handle_quote> bool advance_line(bool spanbuf, bool *inquote);
	void unquote_field(char *p);
	void csv_nullprint_lastfield();
	void unescape_lastfield(bool hasescape);
	bool parseline_csv();
	bool parseline_pgtext();

	bool parseline() {
		m_currline_no += 1;
		if (m_csvmode) {
			return parseline_csv();
		} else {
			return parseline_pgtext();
		}
	}

	int (*m_readfn)(void *hndl, void *buf, int bufsz, char *errmsg, int errsz);
	void *m_readfn_hndl;

	int m_ncol;
	bool m_csvmode;
	bool m_hdrline;
	char m_delim;
	char m_quote;
	char m_escape;
	char m_comment;
	std::string m_nullstr;
	const char *m_nullstr_ptr;

#ifdef USE_SSE 
	__m128i m_xmm_line_search;
	__m128i m_xmm_sep_search;
#else 
	char m_xmm_line_search[16];
	char m_xmm_sep_search[16];
#endif

	std::vector<char*> m_fields;
	std::vector<char> m_rbuf;

	char *m_rbuf_end;
	char *m_currline_begin;
	char *m_currline_end;
	char *m_nextline_begin;
	bool m_eat_next_lf;

	std::string m_currline_copy;
	int m_currline_no;
	int64_t m_nrow;

protected:

	bool start_internal(int (*readfn)(void *hndl, void *buf, int bufsz, char *errmsg, int errsz),
			void *hndl);
	static const int ERRMSGSZ = 512;
	char m_errmsg[ERRMSGSZ]; 
};

}	/* namespace */
#endif


