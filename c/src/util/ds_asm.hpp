#ifndef _DS_ASM_HPP_
#define _DS_ASM_HPP_

#include <unistd.h>
#include <limits.h>
#include <cstring>
#include <time.h>
#include <sys/time.h>
#include <sys/sysinfo.h>

#include "ds_macros.hpp"

#define COMPILER_MEM_BARRIER() __asm__ __volatile__ ("": : :"memory")

namespace dslite {


static inline void asm_pause()
{
	__asm__ __volatile__( "pause" :);
}

static inline void asm_cpuid(uint32_t CPUInfo[4], uint32_t InfoType)
{
	__asm__ __volatile__( "cpuid" :
			"=a" (CPUInfo[0]),
			"=b" (CPUInfo[1]),
			"=c" (CPUInfo[2]),
			"=d" (CPUInfo[3]) :
			"a" (InfoType),
			"c" (0));
}

static inline uint64_t asm_rdtsc_raw()
{
	uint32_t lo, hi;
	__asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
	return (uint64_t)lo | ((uint64_t)hi << 32);
}

/*
 * RDTSC: Issue a CPUID instruction to prevent instruction reordering. 
 */
static inline uint64_t asm_tdtsc()
{
	uint32_t info[4];
	asm_cpuid(info, 0);
	return asm_rdtsc_raw();
}

static inline bool cpu_support_sse42()
{
	// gcc __builtin_cpu_supports("sse4.2");
	uint32_t info[4];
	asm_cpuid(info, 0);
	if (info[0] >= 0x00000001) {
		asm_cpuid(info, 0x00000001);
		return (info[2] & (1U << 20)) != 0;
	}
	return false;
}

static inline int64_t gettime_ns()
{
	const int64_t ns_per_sec = 1000000000; 
	int64_t ns;
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	ns = ts.tv_nsec + ts.tv_sec * ns_per_sec;
	return ns;
}

static inline int64_t gettime_us() { return gettime_ns() / 1000; }
static inline int64_t gettime_ms() { return gettime_ns() / 1000000; }

static inline uint64_t is_power_of_2(uint64_t v) 
{
	return (v & (v-1)) == 0;
}

static inline uint64_t round_up(uint64_t v, uint64_t align)
{
	// align must be pwoer of 2.
	return (v + align - 1) & (~(align - 1));
}
static inline uint64_t is_aligned(uint64_t v, uint64_t align)
{
	return round_up(v, align) == v;
}

static inline char* align_and_zero(char *p, uint64_t align)
{
	uintptr_t up = (uintptr_t) p;
	uint64_t zsz = 0;
	if (is_aligned(up, align)) {
		zsz = round_up(up, align) - up;
		bzero(p, zsz);
	}
	return p+zsz;
}

static inline int builtin_clz32(uint32_t v) 
{
	return __builtin_clz(v);
}
static inline int builtin_clz64(uint64_t v)
{
	return __builtin_clzll(v);
}

}	/* namespace */	
#endif 
