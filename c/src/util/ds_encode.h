#ifndef DSLITE_ENCODE_H
#define DSLITE_ENCODE_H

#if defined (__cplusplus)
extern "C" {
#endif

unsigned hex_encode(const char *src, unsigned len, char *dst);
unsigned hex_decode(const char *src, unsigned len, char *dst);


#if defined (__cplusplus)
}
#endif
#endif

