#ifndef _DSLITE_HASH_HPP_
#define _DSLITE_HASH_HPP_

#include <stdint.h>
#include "xxhash.h"
#include <openssl/sha.h>

namespace dslite {

uint64_t xxhash(const char *data, size_t sz) 
{
	return ::XXH64(data, sz, 0);
}

void sha1(const char* data, size_t sz, char *md)
{
	::SHA1((const unsigned char *) data, sz, (unsigned char *) md);
}

void sha256(const char* data, size_t sz, char *md)
{
	::SHA256((const unsigned char*) data, sz, (unsigned char *) md);
}

}
#endif


