#ifndef _DS_MACROS_HPP_
#define _DS_MACROS_HPP_

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define FLINE __FILE__ ":" TOSTRING(__LINE__)

#define DISALLOW_COPY_AND_ASSIGN(T)				\
private:										\
	T(const T&);				/* =delete; */	\
	void operator=(const T&);	/* =delete; */

#define STRUCT_OF(st, m, p) ((st *) ((char *)p - offsetof(st, m)))

#define UNUSED(x) (void) (x)

#endif 
