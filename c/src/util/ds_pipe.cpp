#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "ds_macros.hpp"
#include "ds_pipe.hpp"

namespace dslite {


int exec_fopen(const char *cmd, int *pipes)
{
	int inpipe[2];
	int outpipe[2];
	int errpipe[2];
	int pid = -1;

	if (::pipe(inpipe) < 0) {
		return -1;
	}

	if (::pipe(outpipe) < 0) {
		::close(inpipe[0]);
		::close(inpipe[1]);
		return -1;
	}

	if (::pipe(errpipe) < 0) {
		::close(inpipe[0]);
		::close(inpipe[1]);
		::close(outpipe[0]);
		::close(outpipe[1]);
		return -1;
	}

	pid = ::fork();
	if (pid > 0) {
		// parent.   Close useless fds
		::close(inpipe[0]);
		::close(outpipe[1]);
		::close(errpipe[1]);
		pipes[0] = inpipe[1];
		pipes[1] = outpipe[0]; 
		pipes[2] = errpipe[0];
	} else if (pid == 0) {
		// child.
		::close(inpipe[1]);
		::close(outpipe[0]);
		::close(errpipe[0]);

		if (::dup2(inpipe[0], fileno(stdin)) < 0) {
			::exit(1);
		}

		if (::dup2(outpipe[1], fileno(stdout)) < 0) {
			::exit(2);
		}

		if (::dup2(errpipe[1], fileno(stderr)) < 0) {
			::exit(3);
		}

		::execl("/bin/sh", "sh", "-c", cmd, NULL);
		// Should never reach here.  If it does, then it is an error.
		::exit(10);
	} else {
		// fork failed.
		::close(inpipe[0]);
		::close(inpipe[1]);
		::close(outpipe[0]);
		::close(outpipe[1]);
		::close(errpipe[0]);
		::close(errpipe[1]);
		return -1;
	}

	// Fork ok.  This is parent.  Return child's pid
	return pid;
}

int exec_fclose(int pid, int *pipes, bool failOnError, char *errbuf, int errbufsz)
{
	int ret = 0;

	if (pid == -1) {
		return 0;
	}

	if (!failOnError) {
		// Rough handling.   But we want to kill ... 
		::kill(pid, SIGKILL);
	}

	if (pipes[0] >= 0) {
		::close(pipes[0]);
		pipes[0] = -1;
	}

	if (pipes[1] >= 0) {
		::close(pipes[1]);
		pipes[1] = -1;
	}

	if (pipes[2] >= 0) {
		if (errbuf) {
			int nread = ::read(pipes[2], errbuf, errbufsz); 
			UNUSED(nread);
		}
		::close(pipes[2]);
		pipes[2] = -1;
	}

	::waitpid(pid, &ret, 0);
	if (ret == 0) {
		// pclose/wait4 OK
		return 0;
	}
	return failOnError ? -1 : 0;
}

}
