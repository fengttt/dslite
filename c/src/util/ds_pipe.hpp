#ifndef _DSLITE_PIPE_H_
#define _DSLITE_PIPE_H_

namespace dslite {

int exec_fopen(const char* cmd, int *pipes);
int exec_fclose(int pid, int *pipes, bool failOnError, char* errbuf, int errbufsz); 

}	/* namespace */
#endif
