#ifndef _DS_SQLITE_H_
#define _DS_SQLITE_H_

#define DS_SET_ERR_MSG(pzErr, ...)				\
	if (1) {									\
		sqlite3_free(*pzErr);					\
		*pzErr = sqlite3_mprintf(__VA_ARGS__);	\
	} else (void) 0 

#define DS_CHECK_COND(cond, pzErr, ...)			\
	if (!(cond)) {								\
		sqlite3_free(*pzErr);					\
		*pzErr = sqlite3_mprintf(__VA_ARGS__);	\
		return SQLITE_ERROR;					\
	} else (void) 0

#define DS_CHECK_MEM(ptr)						\
	if (!(ptr)) {								\
		return SQLITE_NOMEM;					\
	} else (void) 0

#define DS_CHECK_COND_RESULT(cond, ctxt, msg)	\
	if (!(cond)) {								\
		sqlite3_result_error(ctxt, msg, -1);	\
		return;									\
	} else (void) 0 

#endif
