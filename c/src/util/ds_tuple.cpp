#include "ds_tuple.hpp"
#include "ds_hash.hpp"

namespace dslite {

static const char* SQLT_INT = "INT";
static const char* SQLT_BOOL = "BOOL"; 

static const char* SQLT_BIGINT = "BIGINT";
static const char* SQLT_SEC = "SECOND"; 
static const char* SQLT_USEC = "USECOND"; 

static const char* SQLT_REAL = "REAL"; 
static const char* SQLT_DAY = "DAY"; 

static const char* SQLT_TEXT = "TEXT"; 
static const char* SQLT_BLOB = "BLOB";

const char* colt2sqlt(char c)
{
	switch (c) {
		case 'b': 
			return SQLT_BLOB;

		case 'B':
			return SQLT_BOOL;
		case 'i':
			return SQLT_INT; 

		case 'I':
			return SQLT_BIGINT;
		case 'S':		/* sec since epoc */
			return SQLT_SEC;
		case 'U':		/* usec since epoc */
			return SQLT_USEC; 

		case 'f':
		case 'F':
		case 'r':
		case 'R':
			return SQLT_REAL;

		case 'D':		/* day */
			return SQLT_DAY; 

		case 't':
			return SQLT_TEXT;
	}
	// Unknow/Un-supported/NYI
	return 0;
}

bool colt_use_offsetlen(char c)
{
	return c == 'b' || c == 't';
}

int parse_bool(const char *s, int32_t *ret)
{
	if (strcasecmp(s, "t") == 0 ||
		strcasecmp(s, "true") == 0 ||
		strcasecmp(s, "y") == 0 ||
		strcasecmp(s, "yes") == 0 ||
		strcasecmp(s, "on") == 0 ||
		strcasecmp(s, "1") == 0) { 
		*ret = 1;
		return 0;
	} 

	if (strcasecmp(s, "f") == 0 ||
		strcasecmp(s, "false") == 0 ||
		strcasecmp(s, "n") == 0 ||
		strcasecmp(s, "no") == 0 ||
		strcasecmp(s, "off") == 0 ||
		strcasecmp(s, "0") == 0) { 
		*ret = 0;
		return 0;
	} 
	return -1;
}

int parse_int32(const char *s, int32_t *ret)
{
	char *pend;
	errno = 0;
	int64_t i = strtol(s, &pend, 0);
	int32_t v = i;
	if (*pend || i != v || (i == 0 && errno)) {
		return -1;
	}
	*ret = v;
	return 0;
}

int parse_int64(const char *s, int64_t *ret)
{
	char *pend;
	errno = 0;
	*ret = strtoll(s, &pend, 0);
	if (*pend || (*ret == 0 && errno)) {
		return -1;
	}
	return 0;
}

int parse_float(const char *s, float *ret)
{
	char *pend;
	errno = 0;
	*ret = strtof(s, &pend);
	if (*pend || (*ret == 0 && errno)) {
		return -1;
	}
	return 0;
}

int parse_double(const char *s, double *ret)
{
	char *pend;
	errno = 0;
	*ret = strtod(s, &pend);
	if (*pend != 0 || (*ret == 0 && errno)) {
		return -1;
	}
	return 0;
}

int parse_datum(char ct, const char *s, datum_t *ret, std::string& scratch) 
{
	switch (ct) {
		case 'B':
			return parse_bool(s, &ret->b);

		case 'i':
			return parse_int32(s, &ret->i);

		case 'I':
		case 'S':
		case 'U':
			return parse_int64(s, &ret->I);
		
		case 'f':
		case 'F':
		case 'r':
		case 'R':
		case 'D':
			return parse_double(s, &ret->F);

		case 'b':
			// NYI
			return -2;

		case 't':
			ret->v.offset = scratch.size();
			ret->v.len = strlen(s) + 1;
			scratch.append(s, ret->v.len);
			return 0;
	}
	return -1;
}

int Tuple::build(const std::string& cts, const char **vals) 
{
	scratch.resize(cts2sz(cts)); 
	std::string tmp;

	for (uint32_t i = 0; i < cts.size(); i++) {
		if (!vals || vals[i] == 0) {
			pnull(cts)[i] = 1;
			pdatum(cts)[i] = datum_zero();
		} else {
			pnull(cts)[i] = 0;
			datum_t tmp = datum_zero();
			int err = parse_datum(cts[i], vals[i], &tmp, scratch);
			pdatum(cts)[i] = tmp;
			if (err < 0) {
				return err;
			}
		}
	}
	return 0;
}

int Tuple::build(const std::string& cts, sqlite3_value **vals)
{
	scratch.resize(cts2sz(cts)); 

	for (uint32_t i = 0; i < cts.size(); i++) {
		if (!vals || vals[i] == 0) {
			pnull(cts)[i] = 1; 
			pdatum(cts)[i] = datum_zero();
		} else {
			pnull(cts)[i] = 0; 
			datum_t tmp = datum_zero();
			switch (cts[i]) {
				case 'b':
					{
						int nb = sqlite3_value_bytes(vals[i]);
						const void *p = sqlite3_value_blob(vals[i]);
						tmp.v.offset = scratch.size();
						tmp.v.len = nb;
						scratch.append((const char*) p, nb);
						pdatum(cts)[i] = tmp;
					}
					break;
				case 'B':
					{
						int v = sqlite3_value_int(vals[i]);
						if (v == 0) {
							pdatum(cts)[i].b = 0;
						} else {
							pdatum(cts)[i].b = 1;
						}
					}
					break;

				case 'i':
					pdatum(cts)[i].i = sqlite3_value_int(vals[i]);
					break;

				case 'I':
				case 'S':
				case 'U':
					pdatum(cts)[i].I = sqlite3_value_int64(vals[i]);
					break;

				case 'f':
				case 'F':
				case 'r':
				case 'R':
				case 'D':
					pdatum(cts)[i].F = sqlite3_value_double(vals[i]);
					break;

				case 't':
					{
						const char* p = (const char*) sqlite3_value_text(vals[i]);
						tmp.v.offset = scratch.size();
						scratch.append(p); 
						scratch.append(1, 0);
						tmp.v.len = scratch.size() - tmp.v.offset;
						pdatum(cts)[i] = tmp;
					}
					break;

				default:
					return -1;
			}
		}
	}
	return 0;
}

int Tuple::get_col(const std::string& cts, int col, sqlite3_context *ctxt) {
	char *pn = pnull(cts);
	datum_t *pd = pdatum(cts);

	if (col >= cts.size()) {
		return -1;
	}

	if (pn[col]) {
		sqlite3_result_null(ctxt);
		return 0;
	}

	switch (cts[col]) {
		case 'b':
			sqlite3_result_blob(ctxt, poffset(pd[col].v.offset), pd[col].v.len, SQLITE_STATIC); 
			return 0;

		case 't':
			sqlite3_result_text(ctxt, poffset(pd[col].v.offset), -1, SQLITE_STATIC); 
			return 0;

		case 'B':
		case 'i':
			sqlite3_result_int(ctxt, pd[col].i); 
			return 0;

		case 'I':
		case 'S':
		case 'U':
			sqlite3_result_int64(ctxt, pd[col].I); 
			return 0;
		
		case 'r':
		case 'R':
		case 'f':
		case 'F':
		case 'D':
			sqlite3_result_double(ctxt, pd[col].F);
			return 0;

		default:
			return -1;
	}
}


void Tuple::build_hashstr(const std::string& cts, bool *col, std::string& buf) {
	char *pn = pnull(cts);
	datum_t *pd = pdatum(cts);

	for (int i = 0; i < (int) cts.size(); i++) {
		if (col && !col[i]) {
			continue;
		}

		if (pn[i]) {
			buf.append(1, 0xe3);
		} else if (colt_use_offsetlen(cts[i])) { 
			buf.append(scratch, pd[i].v.offset, pd[i].v.len);
		}
	}
}

uint64_t Tuple::xxhash(const std::string& cts, uint64_t seed, bool *col)
{
	std::string buf;
	buf.append((const char *) &seed, sizeof(seed));
	build_hashstr(cts, col, buf);
	return dslite::xxhash(buf.data(), buf.size());
}

void Tuple::sha1(const std::string& cts, bool *col, char *md) 
{
	std::string buf;
	build_hashstr(cts, col, buf);
	dslite::sha1(buf.data(), buf.size(), md);
}

void Tuple::sha256(const std::string& cts, bool *col, char *md) 
{
	std::string buf;
	build_hashstr(cts, col, buf);
	dslite::sha256(buf.data(), buf.size(), md);
}
 
}	/* namespace */

