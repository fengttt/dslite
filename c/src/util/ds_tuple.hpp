#ifndef _DSUTIL_TUPLE_HPP_
#define _DSUTIL_TUPLE_HPP_

#include <string>
#include <vector>
#include <uuid/uuid.h>

#include "ds_asm.hpp"

#include "sqlite3.h"

namespace dslite {

const char* colt2sqlt(char c);
bool colt_use_offsetlen(char c);

struct offlen_t {
	int32_t offset;
	int32_t len;
};

union datum_t {
	int32_t		b;			/* boolean */
	int32_t		i;
	int64_t		I;
	float		f;
	double		F;	
	double		day;		/* ts: julian day number. */ 
	int64_t		sec;		/* sec since unix epoc. */
	int64_t		usec;		/* micro seconds since unix epoc. */
	offlen_t	v;			/* varlen */
};

static_assert(sizeof(datum_t) == 8);
static inline datum_t datum_zero() { 
	datum_t d;
	d.I = 0;
	return d;
}

int parse_bool(const char *s, int32_t *ret);
int parse_int32(const char *s, int32_t *ret);
int parse_int64(const char *s, int64_t *ret);
int parse_float(const char *s, float *ret);
int parse_double(const char *s, double *ret);

int parse_datum(char ct, const char *s, datum_t *ret, std::string& scratch);

struct Tuple {
	std::string scratch; 

	static size_t cts2sz(const std::string& cts) {
		size_t ncol = cts.size(); 
		size_t sz = ncol * sizeof(datum_t) + ncol;
		return round_up(sz, 8);
	}

	char *pnull(const std::string& cts) { return &scratch[0]; } 
	datum_t *pdatum(const std::string& cts) {
		size_t ncol = cts.size(); 
		size_t sz = round_up(ncol, 8);
		char *pp = pnull(cts);
		return (datum_t*) (pp+sz); 
	}
	bool isnull(const std::string& cts, int i) { return pnull(cts)[i] != 0; }
	datum_t datum(const std::string& cts, int i) { return pdatum(cts)[i]; }

	char *poffset(int32_t off) {
		char *p = &scratch[0];
		return p+off;
	}

	int build(const std::string& cts, const char **vals);
	int build(const std::string& cts, sqlite3_value **vals);
	int build_x(const std::string& cts, const char **v1, sqlite3_value **v2) 
	{
		if (v1) {
			return build(cts, v1);
		} 
		return build(cts, v2);
	}

	int get_col(const std::string& cts, int col, sqlite3_context *ctxt); 

	void build_hashstr(const std::string& cts, bool *col, std::string& buf);
	uint64_t xxhash(const std::string& cts, uint64_t seed, bool *col); 
	uint64_t xxhash(const std::string& cts) { return xxhash(cts, 0, 0); }

	uint64_t highwayhash(const std::string& cts, uint64_t seed, bool *col);
	uint64_t highwayhash(const std::string& cts) { return highwayhash(cts, 0, 0); } 

	void sha1(const std::string& cts, bool *col, char *md);
	void sha1(const std::string& cts, char *md) { return sha1(cts, 0, md); }

	void sha256(const std::string& cts, bool *col, char *md);
	void sha256(const std::string& cts, char *md) { return sha1(cts, 0, md); }
};

}	/* namespace */
#endif
