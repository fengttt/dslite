#include <stdlib.h>

#include "ds_tuple.hpp"
#include "ds_window.hpp"
#include "wormhole.hpp"

namespace dslite {
int64_t Window::add_tuple_impl(const char **svals, sqlite3_value **vals)
{
	m_cnt++;
	m_delta.clear();
	Tuple tup;
	tup.build_x(m_cts, svals, vals);

	if (m_wt == WINDOW_N) { 
		if (m_q.size() == m_n) {
			m_delta.push_back(m_q.front());
			m_q.pop_front();
		} 
		m_q.push_back(tup);
		m_delta.push_back(tup);
	} else if (m_wt == WINDOW_RESERVOIR) {
		if (m_vec.size() < m_n) {
			m_vec.push_back(tup);
			m_delta.push_back(tup);
		} else {
			int rr = int(drand48() * m_cnt);
			if (rr < m_n) { 
				m_delta.push_back(m_vec[rr]);
				m_vec[rr] = tup; 
				m_delta.push_back(m_vec[rr]);
			}
		}
	} else if (m_wt == WINDOW_TS) {
		if (tup.isnull(m_cts, 0)) {
			// no ts?
			return -1;
		}
		datum_t ts = tup.datum(m_cts, 0);
		if (ts.F <= 0) {
			// bad ts
			return -1;
		}

		while (!m_q.empty()) {
			if (m_qts.front() + m_ts > ts.F) {
				m_delta.push_back(m_q.front());
				m_q.pop_front();
				m_qts.pop_front();
			} else {
				break;
			}
		}

		m_q.push_back(tup);
		m_delta.push_back(tup);
		m_qts.push_back(ts.F);
	} else {
		// m_wt error
		return -1;
	}
	return m_cnt;
}

int64_t Window::tuples(std::vector<const Tuple*>& vt) {
	if (m_wt == WINDOW_N || m_wt == WINDOW_TS) { 
		for (std::deque<Tuple>::iterator it = m_q.begin(); it != m_q.end(); ++it) {
			vt.push_back(&(*it));
		}
	} else if (m_wt == WINDOW_RESERVOIR) {
		for (std::vector<Tuple>::iterator it = m_vec.begin(); it != m_vec.end(); ++it) {
			vt.push_back(&(*it));
		}
	} else {
		return -1;
	}
	return m_cnt;
}

int64_t Window::deltas(std::vector<const Tuple*>& vt) {
	for (std::vector<Tuple>::iterator it = m_delta.begin(); it != m_delta.end(); ++it) {
		vt.push_back(&(*it));
	}
	return m_cnt;
}

bool register_window_unnest_function(const std::string& s, window_unnest_function_creator c)
{
	return wormhole_add(s, (void *) c, 0);
}

class WindowDefaultUnnestFunction : public WindowUnnestFunction {
public:
	std::vector<const Tuple*>* run(Window *w) 
	{
		m_vec.clear();
		w->tuples(m_vec);
		return &m_vec;
	}

	std::vector<const Tuple*> m_vec;
};

WindowUnnestFunction* create_window_unnest_function(const std::string& s)
{
	if (s == "unnest_default") {
		return new WindowDefaultUnnestFunction();
	} 

	void *p = 0;
	if (!wormhole_get(s, &p)) {
		return 0;
	}

	window_unnest_function_creator c = (window_unnest_function_creator) p;
	return (*c)();
}


}	// namespace.
