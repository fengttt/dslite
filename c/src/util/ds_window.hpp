#ifndef _DSUTIL_WINDOW_HPP_
#define _DSUTIL_WINDOW_HPP_

#include <string>
#include <deque>
#include <vector>
#include <cstdint>

#include "ds_macros.hpp"
#include "ds_tuple.hpp"

namespace dslite {
enum {
	WINDOW_N,
	WINDOW_TS,
	WINDOW_RESERVOIR,
};

class Window {
	DISALLOW_COPY_AND_ASSIGN(Window);

public:
	Window(const std::string& cts, int wt, int n, double ts) 
		: m_cts(cts), m_wt(wt), m_n(n), m_ts(ts), m_cnt(0) {}

	int64_t tuples(std::vector<const Tuple *>& vt);
	int64_t deltas(std::vector<const Tuple *>& vt);
	int64_t add_tuple(const char **vals) { return add_tuple_impl(vals, 0); }
	int64_t add_tuple(sqlite3_value **vals) { return add_tuple_impl(0, vals); }

	int cts_ncol() { return (int) m_cts.size(); }

private:
	std::vector<Tuple> m_vec;
	std::deque<Tuple> m_q;
	std::deque<double> m_qts;
	std::vector<Tuple> m_delta;

	int64_t add_tuple_impl(const char** sval, sqlite3_value **vals);
	std::string m_cts;

	int m_wt;
	unsigned m_n;
	double m_ts;
	int64_t m_cnt;
};

class WindowUnnestFunction {
public:
	virtual std::vector<const Tuple*>* run(Window *w) = 0;
	virtual ~WindowUnnestFunction() {};
};


typedef WindowUnnestFunction* (*window_unnest_function_creator) ();
bool register_window_unnest_function(const std::string& s, window_unnest_function_creator c); 
WindowUnnestFunction *create_window_unnest_function(const std::string& s);

}	/* namespace */
#endif
