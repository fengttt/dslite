#include "ds_asm.hpp"
#include "wormhole.hpp"

#include <mutex>
#include <map>

#include <time.h>
#include <stdlib.h>

namespace dslite {
struct wormhole_entry_t {
	int64_t tsns;
	void *p;
	wormhole_remove_fn fn;

	wormhole_entry_t(void *_p, wormhole_remove_fn _fn) : p(_p), fn(_fn) 
	{ tsns = gettime_ns(); }
	~wormhole_entry_t() { release(); }

	void release() {
		if (fn && p) {
			(*fn)(p);
		}
		p = 0;
		fn = 0;
	} 
};

static std::map<std::string, wormhole_entry_t*> *g_wmh_map;
static std::mutex g_lk;

struct wmh_lock_t {
	wmh_lock_t() { g_lk.lock(); } 
	~wmh_lock_t() { g_lk.unlock(); }
};

static std::map<std::string, wormhole_entry_t*> *wmh_map()
{
	if (g_wmh_map == 0) {
		g_wmh_map = new std::map<std::string, wormhole_entry_t*>();
		// Let me call some other setup stuff here.
		::srand48(::time(0));
	}
	return g_wmh_map;
}

bool wormhole_add(const std::string& key, void *p, wormhole_remove_fn fn)
{
	wmh_lock_t LOCK;
	std::map<std::string, wormhole_entry_t*>::iterator it = wmh_map()->find(key);
	if (it != wmh_map()->end()) {
		// double add considered an error.
		return false;
	}

	wormhole_entry_t *pe = new wormhole_entry_t(p, fn);
	(*wmh_map())[key] = pe;
	return true;
}

bool wormhole_get(const std::string& key, void **pp) 
{
	wmh_lock_t LOCK;
	std::map<std::string, wormhole_entry_t*>::iterator it = wmh_map()->find(key);
	if (it == wmh_map()->end()) {
		*pp = 0;
		return false;
	}
	*pp = it->second->p;
	return true;
}

bool wormhole_remove(const std::string& k) 
{
	wormhole_entry_t *pe = 0;
	{
		wmh_lock_t LOCK;
		std::map<std::string, wormhole_entry_t*>::iterator it = wmh_map()->find(k);
		if (it == wmh_map()->end()) {
			return false;
		}
		pe = it->second;
		wmh_map()->erase(it);
	}

	if (pe) {
		delete pe;
	}
	return true;
}

bool wormhole_list(std::vector<std::string> &kvec, std::vector<int64_t> &tsvec)
{
	wmh_lock_t LOCK;
	for (std::map<std::string, wormhole_entry_t*>::iterator it = wmh_map()->begin(); it != wmh_map()->end(); ++it) {
		kvec.push_back(it->first);
		tsvec.push_back(it->second->tsns);
	}
	return true;
}

}	/* namespace */
