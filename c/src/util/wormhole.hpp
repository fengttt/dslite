#ifndef _DSLITE_WORMHOLE_HPP_
#define _DSLITE_WORMHOLE_HPP_

#include <string>
#include <vector>
#include <cstdint>

namespace dslite {

typedef void (*wormhole_remove_fn)(void *p);

bool wormhole_add(const std::string& key, void *p, wormhole_remove_fn fn);
bool wormhole_get(const std::string& key, void **pp);
bool wormhole_remove(const std::string& key);
bool wormhole_list(std::vector<std::string> &kvec, std::vector<int64_t>& ts);

}		/* namespace */
#endif	/* _HPP_ */
