package dslite

import (
	"database/sql"
	"github.com/mattn/go-sqlite3"
	"log"
)

func OpenDB(dsn string) (*sql.DB, error) {
	if dsn == "" || dsn == ":memory:" {
		dsn = "file:memory.db?cache=shared&mode=memory"
	}

	db, err := sql.Open("dslite3", dsn)
	return db, err
}

func init() {
	sql.Register("dslite3",
		&sqlite3.SQLiteDriver{
			ConnectHook: func(conn *sqlite3.SQLiteConn) error {
				err := conn.LoadExtension("extensionfunctions.so", "sqlite3_extensionfunctions_init")
				if err != nil {
					return err
				}

				err = conn.CreateModule("govt", VtabFactory())
				if err != nil {
					log.Panic("Cannot create govt module. ", err)
					return err
				}

				return nil
			},

			Extensions: []string{
				"series",
				"dslitemisc",
				"dslitecsv",
				"dslitesketch",
				"dslitegraph",
			},
		})
}
