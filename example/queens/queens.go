package main

import (
	"fmt"
	"log"

	"gitlab.com/fengttt/dslite"
)

func main() {
	db, err := dslite.OpenDB(":memory:")
	if err != nil {
		log.Panic("Cannot open database", err)
	}
	defer db.Close()

	qry := `
		select * from generate_series(1, 8) r1,
			generate_series(1, 8) r2,
			generate_series(1, 8) r3,
			generate_series(1, 8) r4,
			generate_series(1, 8) r5,
			generate_series(1, 8) r6,
			generate_series(1, 8) r7,
			generate_series(1, 8) r8
		where 
			r2.value - r1.value != 0 and r2.value - r1.value != 1 and r2.value - r1.value != -1
			and r3.value - r1.value != 0 and r3.value - r1.value != 2 and r3.value - r1.value != -2
			and r4.value - r1.value != 0 and r4.value - r1.value != 3 and r4.value - r1.value != -3
			and r5.value - r1.value != 0 and r5.value - r1.value != 4 and r5.value - r1.value != -4
			and r6.value - r1.value != 0 and r6.value - r1.value != 5 and r6.value - r1.value != -5
			and r7.value - r1.value != 0 and r7.value - r1.value != 6 and r7.value - r1.value != -6
			and r8.value - r1.value != 0 and r8.value - r1.value != 7 and r8.value - r1.value != -7

			and r3.value - r2.value != 0 and r3.value - r2.value != 1 and r3.value - r2.value != -1
			and r4.value - r2.value != 0 and r4.value - r2.value != 2 and r4.value - r2.value != -2
			and r5.value - r2.value != 0 and r5.value - r2.value != 3 and r5.value - r2.value != -3
			and r6.value - r2.value != 0 and r6.value - r2.value != 4 and r6.value - r2.value != -4
			and r7.value - r2.value != 0 and r7.value - r2.value != 5 and r7.value - r2.value != -5
			and r8.value - r2.value != 0 and r8.value - r2.value != 6 and r8.value - r2.value != -6

			and r4.value - r3.value != 0 and r4.value - r3.value != 1 and r4.value - r3.value != -1
			and r5.value - r3.value != 0 and r5.value - r3.value != 2 and r5.value - r3.value != -2
			and r6.value - r3.value != 0 and r6.value - r3.value != 3 and r6.value - r3.value != -3
			and r7.value - r3.value != 0 and r7.value - r3.value != 4 and r7.value - r3.value != -4
			and r8.value - r3.value != 0 and r8.value - r3.value != 5 and r8.value - r3.value != -5

			and r5.value - r4.value != 0 and r5.value - r4.value != 1 and r5.value - r4.value != -1
			and r6.value - r4.value != 0 and r6.value - r4.value != 2 and r6.value - r4.value != -2
			and r7.value - r4.value != 0 and r7.value - r4.value != 3 and r7.value - r4.value != -3
			and r8.value - r4.value != 0 and r8.value - r4.value != 4 and r8.value - r4.value != -4

			and r6.value - r5.value != 0 and r6.value - r5.value != 1 and r6.value - r5.value != -1
			and r7.value - r5.value != 0 and r7.value - r5.value != 2 and r7.value - r5.value != -2
			and r8.value - r5.value != 0 and r8.value - r5.value != 3 and r8.value - r5.value != -3

			and r7.value - r6.value != 0 and r7.value - r6.value != 1 and r7.value - r6.value != -1
			and r8.value - r6.value != 0 and r8.value - r6.value != 2 and r8.value - r6.value != -2

			and r8.value - r7.value != 0 and r8.value - r7.value != 1 and r8.value - r7.value != -1
		`

	rows, err := db.Query(qry)
	if err != nil {
		log.Panic("Error: ", err)
	}

	cnt := 0

	for rows.Next() {
		var r1, r2, r3, r4, r5, r6, r7, r8 int
		err = rows.Scan(&r1, &r2, &r3, &r4, &r5, &r6, &r7, &r8)
		if err != nil {
			log.Panic("Cannot scan rows", err)
		}
		cnt += 1
		fmt.Printf("Row: %d (%d, %d, %d, %d, %d, %d, %d, %d)\n", cnt, r1, r2, r3, r4, r5, r6, r7, r8)
	}
}
