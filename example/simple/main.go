package main

import (
	"fmt"
	"log"

	"gitlab.com/fengttt/dslite"
)

func main() {
	db, err := dslite.OpenDB(":memory:")
	if err != nil {
		log.Panic("Cannot open database", err)
	}

	rows, err := db.Query("select cos(1)")
	if err != nil {
		log.Panic("Error: ", err)
	}

	cnt := 0

	for rows.Next() {
		var x float64
		err = rows.Scan(&x)
		if err != nil {
			log.Panic("Cannot scan rows", err)
		}
		cnt += 1
		fmt.Println("Row: ", x)
	}
}
