module gitlab.com/fengttt/dslite

go 1.14

require (
	github.com/go-delve/delve v1.4.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
)
