import os
import threading, time
import re, tabulate
import apsw 
from pathlib import Path

class Dslite:
    def __init__(self):
        self.dbname = None
        self.dbconn = None
        self.nexttmp = 0
        self.colchk = re.compile(r'^[a-zA-Z]\w*$')

    def nexttmpname(self):
        self.nexttmp += 1
        return 'tmp_{0}'.format(self.nexttmp)

    def opendb(self, db="", libdir=""):
        if db=="":
            db = ":memory:"

        if self.dbconn == None or self.dbname != db:
            if self.dbconn != None:
                self.dbconn.close()
            self.dbname = db
            self.dbconn = apsw.Connection(self.dbname)
            if libdir == '':
                libdir = str(Path.home()) + '/.local/lib'
            self.dbconn.enableloadextension(True)
            self.dbconn.loadextension(libdir + "/series")
            self.dbconn.loadextension(libdir + "/extensionfunctions") 
            self.dbconn.loadextension(libdir + "/dslitemisc") 
            self.dbconn.loadextension(libdir + "/dslitecsv")
            self.dbconn.loadextension(libdir + "/dslitesketch")
            self.dbconn.loadextension(libdir + "/dslitegraph")

        return self

    def name(self):
        return self.dbname

    def conn(self):
        if self.dbconn == None:
            raise Exception("Dslite Error: no connection to " + self.dbname)
        return self.dbconn

    def cur(self):
        # According to apsw, cursor is automatically GCed therefore 
        # no need to close.
        return self.conn().cursor()

    def exec_sql(self, sql, *args):
        return self.cur().execute(sql, *args)

    def tabulate_sql(self, sql, *args, tablefmt="psql"):
        cur = self.cur()
        gg = cur.execute(sql, *args)
        try:
            d = cur.getdescription()
        except:
            return "No/Empty result"
        hdr = [col[0] for col in d]
        return tabulate.tabulate(gg, hdr, tablefmt=tablefmt)

    def exec_row(self, sql, *args):
        cur = self.cur() 
        cur.execute(sql, *args)
        row = cur.fetchone()
        return row

    def exec_value(self, sql, *args):
        row = self.exec_row(sql)
        return row[0]

    def generator_with_desc(self, sql):
        cur = self.cur()
        g = cur.execute(sql)
        d = cur.getdescription()
        return (g, d)

    def generator(self, sql):
        cur = self.cur()
        return cur.execute(sql)

    def castone(self, v, t):
        if t == 'int' or t == 'INT':
            return int(v)
        elif t == 'bigint' or t == 'BIGINT':
            return int64(v)
        elif t == 'real' or t == 'REAL':
            return float(v)
        else:
            return str(v)

    def feature_cast(self, row, d):
        ret = list(row)
        for i in range(len(d)): 
            ret[i] = self.castone(row[i], d[i][1]) 
        return tuple(ret)

    def generator_features_cast(self, sql): 
        if type(sql) is bytes:
            sql = sql.decode('utf-8')
        cur = self.cur()
        gg = cur.execute(sql)
        d = cur.getdescription()
        for row in gg: 
            yield self.feature_cast(row, d) 

    def generator_features(self, sql):
        if type(sql) is bytes:
            sql = sql.decode('utf-8')
        cur = self.cur()
        gg = cur.execute(sql) 
        for row in gg: 
            yield row

    def checkcols(self, cols):
        for col in cols:
            if not self.colchk.match(col):
                raise ValueError('Invalid column name {0}'.format(col))

    def describe(self, cols, sql, *args):
        kllcol = ''
        rescol = ''
        cleanup = ''
        self.checkcols(cols)

        for col in cols:
            kllcol += '''
                count({0}) as {0}_count, 
                avg({0}) as {0}_avg,
                min({0}) as {0}_min,
                max({0}) as {0}_max,
                stdev({0}) as {0}_stdev,
                dslite_floatkll('{0}_desc_kll', {0}) as {0}_fkll,
            '''.format(col)
            rescol += '''
                {0}_count, {0}_avg, {0}_min, {0}_max, {0}_stdev, 
                dslite_floatkll_quantile('{0}_desc_kll', 0.25) as {0}_q1,
                dslite_floatkll_quantile('{0}_desc_kll', 0.5) as {0}_q2,
                dslite_floatkll_quantile('{0}_desc_kll', 0.75) as {0}_q3,
                '''.format(col)
            cleanup += '''
            select dslite_wormhole_del('{0}_desc_kll');
            '''.format(col)

        buildsql = '''
                WITH ORIGSQL AS ({0}), 
                     DESCSQL AS (SELECT {1} count(*) as CNT FROM ORIGSQL)
                SELECT {2} CNT FROM DESCSQL
                '''.format(sql, kllcol, rescol) 

        cur = self.cur()
        cur.execute(buildsql, *args)
        curd = cur.getdescription()

        retrow = cur.fetchone()
        ret = [['column', 'count', 'mean', 'stdev', 'min', '25%', '50%', '75%', 'max']]
        for i in range(len(cols)):
            next = [cols[i]]
            next.extend(retrow[i*8:i*8+8])
            ret.append(next)

        cur.execute(cleanup)
        return ret, retrow[-1]

    def histogram(self, cols, sql, *args, bin=10):
        ret = {}
        self.checkcols(cols)

        for col in cols:
            tmpsql1 = '''
                WITH ORIGSQL AS ({0}) 
                SELECT count({1}), min(cast({1} as real)), max(cast({1} as real)) FROM ORIGSQL
                '''.format(sql, col)
            cnt, minval, maxval = self.exec_row(tmpsql1, *args)
            # trivial case
            if bin <= 1 or cnt <= 1 or minval == maxval: 
                ret[col] = [(0, minval, maxval, cnt)]
            else:
                binwidth = (maxval - minval) / bin
                tmpsql2 = '''
                WITH ORIGSQL AS ({0}),
                     BINSQL AS (SELECT 
                        CASE WHEN {1} = {2} THEN {3} 
                             ELSE cast((cast({1} as real) - {4})/{5} as int) 
                        END AS bin FROM ORIGSQL)
                SELECT bin, count(*) as bincnt 
                FROM BINSQL GROUP BY bin ORDER BY bin
                '''.format(sql, col, maxval, bin-1, minval, binwidth)
                cur = self.cur()
                cur.execute(tmpsql2, *args)
                row = cur.fetchone() 
                hist = []
                for i in range(bin): 
                    lb = minval + i * binwidth
                    ub = lb + binwidth
                    cnt = 0
                    if i == bin - 1:
                        ub = maxval
                    if row is not None:
                        if row[0] == i:
                            cnt = row[1]
                            row = cur.fetchone()
                    entry = (i, lb, ub, cnt)
                    hist.append(entry)
                ret[col] = hist

        return ret

