import pytest
import dslite
import os

@pytest.fixture
def db():
    return dslite.Dslite().opendb()

def datadir():
    pwd = os.path.dirname(__file__)
    return pwd + "/../data"

def test_tr(db):
    sql = "CREATE VIRTUAL TABLE tr1 USING dslite_csv(filename='%s/facebook_combined.txt.gz');" % (datadir()) 
    db.exec_sql(sql) 
    cnt = db.exec_value('select count(*) from tr1')
    assert cnt == 88234

    sql = "CREATE VIRTUAL TABLE tr2 USING dslite_csv(filename='%s/facebook_combined.txt.gz', comment='0');" % (datadir()) 
    db.exec_sql(sql) 
    cnt = db.exec_value('select count(*) from tr2')
    assert cnt == 88234 - 347
