import pytest
import dslite
import os

@pytest.fixture
def db():
    return dslite.Dslite().opendb()

def datadir():
    pwd = os.path.dirname(__file__)
    return pwd + "/../data"

def test_tr(db):
    sql = "CREATE VIRTUAL TABLE tr1 USING dslite_csv(filename='%s/iris_training.csv', header=1);" % (datadir()) 
    db.exec_sql(sql) 
    cnt = db.exec_value('select count(*) from tr1')
    assert cnt == 120

    sql = "CREATE VIRTUAL TABLE tr2 USING dslite_csv(filename='%s/iris_training.csv', header=11);" % (datadir()) 
    db.exec_sql(sql) 
    cnt = db.exec_value('select count(*) from tr2')
    assert cnt == 110

def test_tt(db):
    sql = "CREATE VIRTUAL TABLE tt1 USING dslite_csv(execute='curl -s file://%s/iris_test.csv', columns=5, header=1);" % (datadir()) 
    db.exec_sql(sql) 
    cnt = db.exec_value('select count(*) from tt1')
    assert cnt == 30

def test_ctyps(db):
    sql = """
        CREATE VIRTUAL TABLE tsct USING dslite_csv(filename='%s/iris_test.csv', 
            coltypes='ffffi',
            header=1);
    """ % (datadir()) 
    db.exec_sql(sql) 
    cnt = db.exec_value('select count(*) from tsct')
    assert cnt == 30
    row = db.exec_row('select sum(c0), sum(c1), sum(c2), sum(c3), sum(c4) from tsct')
    assert row[0] > 175 and row[0] < 176 
    assert row[1] > 90 and row[1] < 91
    assert row[2] > 115 and row[1] < 116 
    assert row[3] > 36 and row[3] < 37
    assert row[4] == 30

def test_schema(db):
    sql = """
        CREATE VIRTUAL TABLE ts1 USING dslite_csv(execute='curl -s file://%s/iris_test.csv', 
            coltypes='ffffi', header =1);
    """ % (datadir()) 
    db.exec_sql(sql) 
    cnt = db.exec_value('select count(*) from ts1')
    assert cnt == 30

    sql = """
        CREATE VIRTUAL TABLE ts2 USING dslite_csv(filename='%s/iris_test.csv', 
            coltypes='ffffi', columns=5, header=1);
    """ % (datadir()) 
    db.exec_sql(sql) 
    cnt = db.exec_value('select count(*) from ts1')
    assert cnt == 30
