import pytest
import dslite

@pytest.fixture
def db():
    db = dslite.Dslite().opendb()
    db.exec_sql('create table ijt(i int, j int, t text)');
    db.exec_sql('''insert into ijt select value, value * 2, 'foo' || value from generate_series(1, 1000)''')
    return db

def test_aaa(db):
    cnt = db.exec_value('select count(*) from ijt') 
    assert cnt == 1000
    row = db.exec_row('select min(i), max(i), min(j), max(j), min(t), max(t) from ijt') 
    assert row[0] == 1 and row[1] == 1000
    assert row[2] == 2 and row[3] == 2000
    assert row[4] == 'foo1' and row[5] == 'foo999'

def test_describe(db):
    s = db.describe(['i'], 'select * from ijt')
    print(s)
    print()
    s = db.describe(['i', 'j'], 'select * from ijt where j <= 1000')
    print(s)
    print()

def test_hist(db):
    s = db.histogram(['i'], 'select * from ijt')
    print(s)
    print()
    s = db.histogram(['i', 'j'], 'select * from ijt where j <= 1000', bin=20)
    print(s)
    print()

