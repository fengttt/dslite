import pytest
import dslite

@pytest.fixture
def db():
    db = dslite.Dslite().opendb()
    db.exec_sql('create table ijt(i int, j int, t text)');
    db.exec_sql('''insert into ijt select value, value * 2, 'foo' || value from generate_series(1, 1000)''')
    db.exec_sql('create virtual table unnest_ijt using dslite_window_unnest(iit)')
    return db

def test_aaa(db):
    cnt = db.exec_value('select count(*) from ijt') 
    assert cnt == 1000
    row = db.exec_row('select min(i), max(i), min(j), max(j), min(t), max(t) from ijt') 
    assert row[0] == 1 and row[1] == 1000
    assert row[2] == 2 and row[3] == 2000
    assert row[4] == 'foo1' and row[5] == 'foo999'

def test_win_n(db): 
    tt = db.tabulate_sql('''select u.winid, u.* from ( select window_n('bar', 'iit', 2, i, j, t) as id from ijt where i < 100) win, unnest_ijt('bar', win.id) u limit 5''') 
    print(tt)
    print()

def test_sample(db):
    db.exec_sql('''select window_reservoir('aaa', 'iit', 1, i, j, t) from ijt''')
    row = db.exec_row('''select * from unnest_ijt('aaa', 1)''')
    print(row)
    print()
    assert row[0] * 2 == row[1]
    assert 'foo' + str(row[0]) == row[2]

def test_sample10(db):
    db.exec_sql('''select max(window_reservoir('bbb', 'iit', 10, i, j, t)) from ijt''')
    tt = db.tabulate_sql('''select * from unnest_ijt('bbb', 1)''')
    print(tt)
    print()
    cnt = db.exec_value('''select count(*) from unnest_ijt('bbb', 1)''')
    assert cnt == 10

def test_sample10000(db):
    db.exec_sql('''select max(window_reservoir('sample10000', 'iit', 10000, i, j, t)) from ijt''')
    cnt = db.exec_value('''select count(*) from unnest_ijt('sample10000', 1)''')
    assert cnt == 1000
