import pytest
import dslite

@pytest.fixture(scope="module")
def db():
    return dslite.Dslite().opendb()

def test_ins(db):
    db.exec_sql("create table tt(i int, t text)")
    db.exec_sql("insert into tt values(1, 'foo'); insert into tt values (2, 'bar');")

def test_a(db):
    v = db.exec_value('select 1')
    assert v == 1

def test_b(db):
    foo = db.exec_value('select t from tt where i = 1')
    assert foo == 'foo'

def test_c(db):
    bar = db.exec_value('select t from tt where i > 1')
    assert bar == 'bar'

