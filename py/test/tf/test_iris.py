import tensorflow as tf
import numpy as np
import pytest
import dslite
import os

from .tftestcommon import to_tf_types 

@pytest.fixture
def db():
    irisdb = dslite.Dslite().opendb()

    datadir = os.path.dirname(__file__) + "/../data"

    sql = """
        CREATE VIRTUAL TABLE iris_training_csv USING dslite_csv(filename='%s/iris_training.csv', 
            coltypes='ffffi', header=1);
        CREATE VIEW iris_training AS select c0 as SepalLength, c1 as SepalWidth,
                                            c2 as PetalLength, c3 as PetalWidth,
                                            c4 as Species from iris_training_csv;

        CREATE VIRTUAL TABLE iris_test_csv USING dslite_csv(filename='%s/iris_test.csv', 
            coltypes='ffffi', header=1);
        CREATE VIEW iris_test AS select c0 as SepalLength, c1 as SepalWidth,
                                            c2 as PetalLength, c3 as PetalWidth,
                                            c4 as Species from iris_test_csv;
    """ % (datadir, datadir) 
    irisdb.exec_sql(sql) 
    return irisdb

def test_cnt(db):
    traincnt = db.exec_value('select count(*) from iris_training')
    assert traincnt == 120
    testcnt = db.exec_value('select count(*) from iris_test')
    assert testcnt == 30

def test_description(db):
    g, d = db.generator_with_desc('select * from iris_test')
    assert len(d) == 5
    assert d[0][1] == 'REAL'
    assert d[1][1] == 'REAL'
    assert d[2][1] == 'REAL'
    assert d[3][1] == 'REAL'
    assert d[4][1] == 'INT' 

    sum = 0
    for (a, b, c, d, e) in g:
        sum += e

    g2 = db.generator_features_cast('select * from iris_test') 
    sum2 = 0
    for (a, b, c, d, e) in g2:
        sum2 += e 

    sumchk = db.exec_value('select sum(Species) from iris_test')
    assert sumchk == sum
    assert sumchk == sum2

def test_dataset(db):
    ds1 = tf.data.Dataset.from_generator(db.generator_features_cast, to_tf_types('ffffi'), args=('select * from iris_test',))
    ds2 = ds1.repeat(2)
    it2 = ds2.make_one_shot_iterator()
    sum2 = 0
    for (a, b, c, d, e) in it2:
        sum2 += e
    sumchk = db.exec_value('select sum(Species) from iris_test')
    assert sumchk * 2 == sum2

def input_fn(db, training=True, batch_size=256):
    if training:
        ds = tf.data.Dataset.from_generator(db.generator_features_cast, to_tf_types('ffffi'), args=(['select * from iris_training']))
        ds = ds.shuffle(1000).repeat()
    else:
        ds = tf.data.Dataset.from_generator(db.generator_features_cast, to_tf_types('ffffi'), args=(['select * from iris_test']))
    ds = ds.map(lambda a,b,c,d,e: ({'SepalLength': a, 'SepalWidth': b, 'PetalLength': c, 'PetalWidth': d}, [e]))
    ds = ds.batch(batch_size)
    return ds

def test_train(db):
    my_feature_columns = []
    for key in ['SepalLength', 'SepalWidth', 'PetalLength', 'PetalWidth']:
        my_feature_columns.append(tf.feature_column.numeric_column(key=key))

    classifier = tf.estimator.DNNClassifier(
            feature_columns = my_feature_columns,
            hidden_units = [30, 10],
            n_classes = 3)
    classifier.train(
            input_fn = lambda: input_fn(db, training=True),
            steps=1000)

    eval_result = classifier.evaluate(
            input_fn = lambda: input_fn(db, training=False))
    assert eval_result['accuracy'] > 0.5
