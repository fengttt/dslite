from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np
import pytest
import dslite
import os

import tensorflow.compat.v2.feature_column as fc
import tensorflow as tf

from .tftestcommon import to_tf_types 

#
# Lots of things to learn in this exercise.
#
@pytest.fixture
def db():
    db = dslite.Dslite().opendb()
    sqltmp = """
        CREATE VIRTUAL TABLE t{0}_csv USING dslite_csv(execute='curl -s https://storage.googleapis.com/tf-datasets/titanic/{0}.csv', coltypes='itriirttttt', header=1);
        CREATE VIEW t{0} as SELECT c0 as survived, c1 as sex, c2 as age, 
                                   c3 as n_siblings_spouses, c4 as parch, c5 as fare,
                                   c6 as class, c7 as deck, c8 as embark_town, c9 as alone
                FROM t{0}_csv
    """

    sql = sqltmp.format('train',)
    db.exec_sql(sql) 
    sql = sqltmp.format('eval',)
    db.exec_sql(sql) 
    return db

def test_aaa(db):
    tcnt = db.exec_value('select count(*) from ttrain')
    assert tcnt == 627
    vcnt = db.exec_value('select count(*) from teval')
    assert vcnt == 264
    desc = db.describe(['age', 'n_siblings_spouses', 'parch', 'fare'], 'select * from ttrain')
    hist = db.histogram(['age'], 'select * from ttrain', bin=20)
    row10 = db.exec_sql('select * from ttrain limit 10')
    print(list(row10))
    print()


def make_input_fn(db, training=True, num_epochs=10, batch_size=32):
    def input_func(): 
        # Make input fn -- argument to to_tf_types, should match table schema. 
        if training:
            ds = tf.data.Dataset.from_generator(db.generator_features_cast, to_tf_types('itfiiftttt'), args=('select * from ttrain',))
            ds = ds.shuffle(1000)
        else:
            ds = tf.data.Dataset.from_generator(db.generator_features_cast, to_tf_types('itfiiftttt'), args=('select * from teval',))
        ds = ds.map(lambda c0,c1,c2,c3,c4,c5,c6,c7,c8,c9: ({
            'sex':c1, 'age':c2, 
            'n_siblings_spouses': c3, 'parch':c4, 
            'fare':c5, 'class':c6,
            'deck':c7, 'embark_town':c8, 'alone':c9}, [c0]))
        ds = ds.batch(batch_size).repeat(num_epochs)
        return ds
    return input_func

def test_f1(db):
    CATEGORICAL_COLUMNS = ['sex', 'n_siblings_spouses', 'parch', 'class', 'deck', 'embark_town', 'alone']
    NUMERIC_COLUMNS = ['age', 'fare']
    feature_columns = []
    for fname in CATEGORICAL_COLUMNS:
        gg = db.exec_sql('select distinct {0} from ttrain'.format(fname,))
        #
        # Notice here, ttrain get data back as string -- sqlite does not enforcing types.
        # However, tensorflow vocabulary cares, therefore, must do proper type cast.
        # 
        # rows returned from exec_sql is a tuple of 1 element, so we must use g[0] to decode it.
        #
        if fname == 'n_siblings_spouses' or fname == 'parch':
            vocabulary = [int(g[0]) for g in gg] 
        else:
            vocabulary = [g[0] for g in gg] 
        feature_columns.append(tf.feature_column.categorical_column_with_vocabulary_list(fname, vocabulary))
    for fname in NUMERIC_COLUMNS:
        feature_columns.append(tf.feature_column.numeric_column(fname, dtype=tf.float32))

    ds = make_input_fn(db, training=True, batch_size=10)()
    for feature_batch, label_batch in ds.take(1):
        print('Some feature keys:', list(feature_batch.keys()))
        print()
        print('A batch of class:', feature_batch['class'].numpy())
        print()
        print('A batch of sex:', feature_batch['sex'].numpy())
        print()
        print('A batch of n_siblings_spouses:', feature_batch['n_siblings_spouses'].numpy())
        print()
        print('A batch of alone:', feature_batch['alone'].numpy())
        print()
        print('A batch of age:', feature_batch['age'].numpy())
        print()
        print('A batch of parch:', feature_batch['parch'].numpy())
        print()
        print('A batch of labels:', label_batch.numpy())
        print()

    age_column = feature_columns[7]
    aa = tf.keras.layers.DenseFeatures([age_column])(feature_batch).numpy()
    print(aa)
    print()

    gender_column = feature_columns[0]
    ga = tf.keras.layers.DenseFeatures([tf.feature_column.indicator_column(gender_column)])(feature_batch).numpy()
    print(ga)
    print()

    est = tf.estimator.LinearClassifier(feature_columns=feature_columns)
    train_input_fn = make_input_fn(db, training=True)
    est.train(train_input_fn) 
    eval_input_fn = make_input_fn(db, training=False, num_epochs=1)
    res = est.evaluate(eval_input_fn) 
    assert res['accuracy'] > 0.6

