import tensorflow as tf

def to_tf_types(tt):
    ret = []
    for t in tt:
        if t == 'b':
            ret.append(tf.bool)
        elif t == 'i':
            ret.append(tf.int32)
        elif t == 'I':
            ret.append(tf.int64)
        elif t == 'f':
            ret.append(tf.float32)
        elif t == 'F':
            ret.append(tf.float64)
        else:
            ret.append(tf.string)
    return tuple(ret)

